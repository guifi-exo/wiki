# org-beamer-export-to-pdf


# #####
# basic
# #####
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
# #+BEAMER_FRAME_LEVEL: 3
# #+BEAMER_FRAME_LEVEL: 3
# #+OPTIONS: H:3n
#+OPTIONS: H:3 num:t toc:nil \n:nil todo:nil pri:nil @:t ::t |:t ^:{} _:{} *:t TeX:t LaTeX:t
# #+OPTIONS: H:1 num:t toc:nil \n:nil todo:nil pri:nil @:t ::t |:t ^:{} _:{} *:t TeX:t LaTeX:t
# source: http://orgmode.org/manual/Beamer-export.html

# #####
# style
# #####
#+latex_header: \mode<beamer>{\usetheme{Goettingen}}
# #+latex_header: \mode<beamer>{\usetheme{Hannover}}
# #+BEAMER_THEME: Gottingen
#+latex_header: \mode<beamer>{\usepackage{times}}
#+latex_header: \setbeamertemplate{footline}[page number]{}
#+latex_header: \setbeamerfont{page number in head/foot}{size=\small}}
# In increasing order: \tiny, \scriptsize, \footnotesize, \small, \normalsize (default), \large, \Large, \LARGE, \huge and \Huge.
# http://www.latex-community.org/forum/viewtopic.php?f=4&t=14510
# #+latex_header: \setbeamertemplate{footline}[frame number]
#+latex_header: \setbeamertemplate{navigation symbols}{}
# http://angel-de-vicente.blogspot.com.es/2013/04/presentations-with-org-mode-beamer.html
# Beamer theme gallery: Goettingen default default
# \usetheme{Goettingen}
# http://www.latex-community.org/forum/viewtopic.php?f=4&t=13865
# http://orgmode.org/worg/exporters/beamer/tutorial.html
# presenting the sections
# disable it!
# this is very formal
# #+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Chapter}\tableofcontents[currentsection]\end{frame}}

# #####
# data
# #####
#+TITLE:     exo.cat contributions in hack4glarus 2019
# #+latex_header: \subtitle{null}
#+AUTHOR:    Roger Garcia and Pedro
#+DATE:      2019-12-01 Sunday
# #+DESCRIPTION: 
# #+KEYWORDS: 
# #+LANGUAGE:  en
# #+OPTIONS:   H:2 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
# #+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
# #+EXPORT_SELECT_TAGS: export
# #+EXPORT_EXCLUDE_TAGS: noexport
# #+LINK_UP:   
# #+LINK_HOME:

* who we are
*** who we are

- exo.cat members (not for profit ISP) that is part of the community network (guifi.net)
  - mostly runned by volunteers, /sometimes/ we get paid

- in guifi.net we are a community with users, volunteers and professionals, with similar interactions to open source and free software projects but in the field of telecommunications and deploying networks (forks are more expensive)

- you might know other community networks like: freifunk.net (germany), funkfeuer.at (austria), ffdn.org (france), ninux (italy). Some of us participate in battlemesh.org (international meeting for community networks). Do you have something like this in Switzerland? :)

* internet access (ISP perspective)

*** internet access (ISP perspective)

accelppp is a tunnel provider for PPPoE and L2TP. We install it from sources (https://gitlab.com/guifi-exo/wiki/blob/master/howto/l2tp-server/accel-ppp.md)

- connect it with prometheus and the snmp exporter
- upgrade from 1.11 to 1.12, from debian stretch to buster
- TODO: include the documentation (maybe howto/prometheus.md)

* request-tracker

*** request-tracker

helpdesk tool for users reporting that internet is working (at the moment we use humans to replace prometheus)

we install from debian package documented here https://gitlab.com/guifi-exo/wiki/blob/master/howto/request-tracker.md

tested and added to documentation:

- migrate from MySQL to PostgreSQL
- upgrade from debian stretch to buster

* ipv6 issues in our org

*** ipv6 issues in our org

- ipv6 communication problems between ungleich.ch and guifi.net
  - solution: our wholesaler provider (fundacio.guifi.net) applied a static route to select a specific carrier that does the good job
- TODO / open issue: we detected a loop in our routing of IPv6
  - we did a blackhole
  - TODO we should emit an /unreachable network/ (we use vyos.io)
    - maybe we require to have /full table/ (participate directly on the internet)
- better announcing the IPv6 network in ripe database

thanks nico (ungleich.ch) for helping us with guidance in ipv6

* cdist

*** cdist

cdist orchestrator looks great for us, unfortunately we could not spend time on it

- lines of code (remembers me to the situation of OpenVPN vs wireguard.io)
  - we use ansible ansible (1 million lines of code in python)
  - but cdist has 30k lines of code: 12k shell, 9k rst, 8k python

- we have nearly 200 openwrt-based mesh devices http://dsg.ac.upc.edu/qmpmon that should be orchestrated (firmware flashing, configuration changes, file changes)

thanks nico (ungleich.ch) for the crash course in cdist
