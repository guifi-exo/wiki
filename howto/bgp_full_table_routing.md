seeing what operators do as of 2021-1

# NYC Mesh

Debian linux with either FRR ( Free Range Routing ) or BIRD. Both are OK but FRR is a little bit better for public internet BGP because of the way it filters, easier to not make a mistake.

It's not very hard, linux is able to take a full table into the inside routing table with no problem (also ipv6). Just make sure to secure the computer very well, and "ACCEPT" "FORWARD" in iptables.

Right now it's not documented, but it could be done in the future.

# rezine.org

rezine.org is part of Grenode. Other non-profits are part of Grenode:

https://www.grenode.net/Pr%C3%A9sentation/ (at the end)

They manage this part of the infrastructure at a higher level because it costs lots of money and requires specific knowledge.  Also it's easier to negotiate contracts using a single entity (Grenode), and then we share it among Grenode members.

They have two options for redundancy with members: either BGP (what they use with Rézine) or VRRP (for single VMs or for members that only do hosting).  They use keepalived for VRRP.  They have some documentation here but it's not up-to-date:

  https://www.grenode.net/Documentation_technique/R%C3%A9seau/Livraisons_IP/

## bgp full table routing

It's good for autonomy to have the full table!

They have been running with the full table for maybe 10 years, first with Quagga.

They switched to Bird 1.X in 2015 (I think when we enabled IPv6). It's much better to work with than Quagga, and really stable. We haven't had any big problem in 6 years.

Simplified configuration here:

https://www.grenode.net/Documentation_technique/R%C3%A9seau/Routage/

For BGP blackholing (RTBH):

https://www.grenode.net/Documentation_technique/R%C3%A9seau/Blackhole/

Gitoyen has almost the same configuration (rezine actually inspired from it and adapted it a bit).  It is public here:

  https://code.ffdn.org/gitoyen/bird-config

The most important part with policies is here:

  https://code.ffdn.org/gitoyen/bird-config/-/blob/master/etc/local/bird/common/bgp-filters.conf

This part is complex, you should start with a simpler configuration first if you want to learn. But at some point you will want to refactor things, and this is a nice way to do it.

We haven't played yet with Bird 2, it brings significant changes with this new thing called "channels".  But I think the filtering part works more or less the same than Bird 1.X.

## hardware

The hardware for the BGP routers:

- https://www.grenode.net/Documentation_technique/Machines/safran/#matriel
- https://www.grenode.net/Documentation_technique/Machines/batture/#matriel

The second one is more recent (more RAM, 10G NIC), they plan to change the first one this year to have 10G.

8 GB of memory should be enough for many years, we bought 16 GB to be really sure to never run out of RAM. Currently they have about 4 million IPv4 routes (several transit + peering) and it takes less than 1 GB RAM :

```
bird> show route  count
3940947 of 3940947 routes for 825925 networks
bird> show route  count primary
825925 of 825925 routes for 825925 networks
```

```
bird> show memory
BIRD memory usage
Routing tables:    315 MB
Route attributes:  346 MB
ROA tables:        192  B
Protocols:         634 kB
Total:             662 MB
```

```
$ ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
bird      4849  2.4  8.7 713700 712820 ?       Ss    2019 14820:17 /usr/sbin/bird -f -u bird -g bird
bird      4850  0.3  1.9 157856 157044 ?       Ss    2019 2171:27 /usr/sbin/bird6 -f -u bird -g bird
```

RSS is 712 MB for IPv4 and 157 MB for IPv6
