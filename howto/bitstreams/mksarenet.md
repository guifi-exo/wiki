# Configuració de Mikrotik RBD52G-5HacD2HnD per Sarenet

S'ha de tenir accès a Internet.

Endollar i accedir a 192.168.88.1

La primera vegada accedeix automàticament a la configuració **[Quick Set]**

## Firmware ##

Comprobar la versió del firmware.

![Imatge](img-doc/mk01.png)

Per instal.lar la recomanada (actualment: v6.46.1 (stable)):

Desde la mateixa pàgina de **[Quick SeT]** a l’apartat **System** a **[Check for Updates]** accedirem a instal.lar el nou firmware:

![Imatge](img-doc/mk02r.png)

Aquest procès finalitza en uns minuts i cal tornar a entrar.

## Password i Identificació ##

Entrarà aut. en la mateixa pantalla **[Quick Set]** aprofitarem i posarem el password.

Identificarem que el n/s i les MAC Address estiguin correctes en la base del router:

![Imatge](img-doc/mk05s.png)

El n/s el trobarem a **System**/**License**

![Imatge](img-doc/mk03.png)

Les MAC a **Interfaces**:

![Imatge](img-doc/mk04.png)

La MAC de la wlan2 la farem servir per identificar el router: a **System**/**Identity**

![Imatge](img-doc/mk06.png)

## Wifis ##

Afegim una password general per la wifi: a **Wireless**/**Security Profiles**

![Imatge](img-doc/mk07.png)

![Imatge](img-doc/mk08.png)

Configurem les wifis (**Advanced Mode**) amb:
- SSID: eXO_nnnnnn_2G (i5G) a on nnnnnn son el 6 ult. Digits de la MAC respectiva
- Wireless Protocol: 802.11 (*fins ara venia per defecte: any*)
- Security profile: el que acaben de crear
- Frequency Mode: regulatory-domain
- Country: spain
- Antenna Gain: 3 dBi
- Distance: indoors (*fins ara venia per defecte: dynamic*)

## Bridges ##

Porta per defecte un bridge que modificarem de nom a «**lan**» per diferenciar-lo de l’altre bridge que crearem.

Creem un nou bridge: «**bstrm**» amb els valors per defecte.

## Interfaces ##

Creem una nova interficie tipus VLAN:
- Name: `ether1.24`
- VLAN ID: 24
- Interface: `ether1`

Un cop creada ja la podem afegir a un nou Port al bridge "bstrm"

Crearem dues noves interfaces tipus PPPoE:

Una tipus PPPoE Client:
- Name: `exo`
- Interfaces: `bstrm`
- User: `id_user@exo.cat`
- Password: laquesigui
- Profile: Default
- Keepalive Timeout: 30
- Use Peer DNS: yes
- Add Default Route: yes

Un altre per gestió, tipus PPPoE Client:
- Name: `mngt`
- Interfaces: `bstrm`
- User: `m_user@exo.cat`
- Password: la mateixa del user
- Profile: default
- Keepalive Timeout: 30
- Use Peer DNS: no
- Add Default Route: no

## Prioritat ##

Hem d’afegir prioritat, es fa a nivell de filtre pel bridge "bstrm"

![Imatge](img-doc/mk11.png)

Nou filtre:
- Chain: output
- Bridges / Out. Bridge: `bstrm`
- Action: set priority
- New Priority: 3

## Interface List ##

Comprobem i modifiquem a **Interfaces** / **Interface List**:

![Imatge](img-doc/mk12.png)

De forma que quedi:

![Imatge](img-doc/mk13.png)

## IPv6 ##

Cal assegurar-se que el paquet esta instal.lat i activat

![Imatge](img-doc/mk19.png)

Si no es així requereix un reboot per activar-lo

![Imatge](img-doc/mk19b.png)

Crear a **IPv6** un nou **DHCP Client** amb els valors:

- Interface: `exo`
- Request: prefix
- Pool Name: exo-prefix
- Pool Prefix Lengt: 56
- Prefix Hint: ::/0

![Imatge](img-doc/mk16.png)

Un cop activat el DHCP Client ipv6 es crearà aut. un **Pool** que s'utilitzarà per crear la nova adreça a **Addresses**

En la nova adreça relacionarem el pool creat i la interface «lan»

![Imatge](img-doc/mk15.png)

( *completar backup i export, i proves* )


## Configuració pel servei de Sarenet-Orange

Sarenet ofereix un servei de pseudo-L2WAP.


La configuració de referència és:

```
/interface bridge
add admin-mac=74:4D:28:80:C8:29 auto-mac=no comment=defconf name=lan
/interface ethernet
set [ find default-name=ether1 ] mtu=1592
/interface eoip
add mac-address=FE:EF:2D:A4:3C:69 mtu=1500 name=bstrm remote-address=10.254.254.254 tunnel-id=206
/interface vlan
add interface=ether1 mtu=1588 name=ether1.23 vlan-id=23
/interface pppoe-client
add add-default-route=yes allow=chap,mschap1,mschap2 disabled=no interface=bstrm keepalive-timeout=30 max-mru=1480 max-mtu=1480 name=exo password=mbHYW7HLrX use-peer-dns=yes \
    user=i0078c0@exo.cat
add disabled=no interface=bstrm keepalive-timeout=30 max-mru=1480 max-mtu=1480 name=mngt password=mbHYW7HLrX user=m0078c0@exo.cat
/interface list
add comment=defconf name=WAN
add comment=defconf name=LAN
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
add authentication-types=wpa2-psk eap-methods="" management-protection=allowed mode=dynamic-keys name=wifi_secrets supplicant-identity="" wpa2-pre-shared-key=facilDrec0rdar.
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-g/n channel-width=20/40mhz-XX country=spain disabled=no mode=ap-bridge name=wlan3 security-profile=wifi_secrets ssid=eXO_9607D5_2G
set [ find default-name=wlan2 ] band=5ghz-a/n/ac channel-width=20/40/80mhz-XXXX country=spain disabled=no frequency=5200 mode=ap-bridge name=wlan4 security-profile=wifi_secrets \
    ssid=eXO_9607D5_5G
/ip hotspot profile
set [ find default=yes ] html-directory=flash/hotspot
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=lan name=defconf
/user group
set full policy=local,telnet,ssh,ftp,reboot,read,write,policy,test,winbox,password,web,sniff,sensitive,api,romon,dude,tikapp
/interface bridge port
add bridge=lan comment=defconf interface=ether3
add bridge=lan comment=defconf interface=ether4
add bridge=lan comment=defconf interface=ether5
add bridge=lan comment=defconf interface=wlan3
add bridge=lan comment=defconf interface=wlan4
add bridge=lan interface=ether2
/ip neighbor discovery-settings
set discover-interface-list=LAN
/interface list member
add interface=lan list=LAN
add interface=exo list=WAN
add interface=mngt list=LAN
add interface=ether2 list=WAN
add interface=ether1.23 list=LAN
/ip address
add address=192.168.88.1/24 comment=defconf interface=lan network=192.168.88.0
/ip dhcp-client
add add-default-route=no disabled=no interface=ether1.23 use-peer-dns=no use-peer-ntp=no
add add-default-route=no !dhcp-options interface=ether2 use-peer-dns=no use-peer-ntp=no
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router.lan type=A
/ip firewall filter
add action=accept chain=input comment="defconf: accept established,related,untracked" connection-state=established,related,untracked
add action=drop chain=input comment="defconf: drop invalid" connection-state=invalid
add action=accept chain=input comment="defconf: accept ICMP" protocol=icmp
add action=drop chain=input comment="defconf: drop all not coming from LAN" in-interface-list=!LAN
add action=accept chain=forward comment="defconf: accept in ipsec policy" ipsec-policy=in,ipsec
add action=accept chain=forward comment="defconf: accept out ipsec policy" ipsec-policy=out,ipsec
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
add action=accept chain=forward comment="defconf: accept established,related, untracked" connection-state=established,related,untracked
add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new in-interface-list=WAN
/ip firewall nat
add action=masquerade chain=srcnat comment="defconf: masquerade" ipsec-policy=out,none out-interface-list=WAN
/ip ssh
set allow-none-crypto=yes forwarding-enabled=remote
/ipv6 address
add address=::764d:28ff:fe80:c829 from-pool=exo-prefix interface=lan
/ipv6 dhcp-client
add add-default-route=yes interface=exo pool-name=exo-prefix pool-prefix-length=56 request=prefix use-peer-dns=no
/ipv6 firewall filter
add action=accept chain=input protocol=icmpv6
add action=accept chain=input dst-port=546 in-interface-list=WAN protocol=udp
add action=accept chain=input connection-state=established,related
add action=drop chain=input
add action=accept chain=forward connection-state=established,related
add action=drop chain=forward connection-state=!established,related in-interface-list=WAN
/ipv6 nd
set [ find default=yes ] advertise-dns=no interface=lan
/system clock
set time-zone-name=Europe/Madrid
/system identity
set name=eXO_9607D5_hAPac2
/tool mac-server
set allowed-interface-list=LAN
/tool mac-server mac-winbox
set allowed-interface-list=LAN
```