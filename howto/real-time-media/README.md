## Conferences

generic

- [jitsi-meet](./jitsi-meet.md) uses [jitsi-videobridge SFU](https://github.com/jitsi/jitsi-videobridge)
- [mumble web client](https://github.com/Johni0702/mumble-web)
  - [install mumble server](https://tec.lleialtat.cat/howtos/mumble)
- [multiparty-meeting](https://github.com/havfo/multiparty-meeting) fork of mediasoup demo, uses [mediasoup SFU](https://github.com/versatica/mediasoup)

education

- [bigbluebutton](https://bigbluebutton.org/) [uses kurento SFU](https://doc-kurento.readthedocs.io/en/6.10.0/user/about.html)
  - it is important to follow check all the steps [here](https://docs.bigbluebutton.org/2.2/troubleshooting)
- [canvas](https://github.com/instructure/canvas-lms/)

## Streaming

- [peerstreamer-ng](https://github.com/netCommonsEU/PeerStreamer-ng) p2p streaming, uses [janus SFU](https://janus.conf.meetecho.com/videoroomtest.html)
