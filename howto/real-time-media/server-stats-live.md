# network monitoring

## goaccess + gwsocket
References:
- for systemd and other https://bytes.fyi/real-time-goaccess-reports-with-nginx/
- for goaccess optons https://ansonvandoren.com/posts/goaccess-nginx-auth/

software:
- https://debian.org
- https://www.nginx.com/
- https://goaccess.io/
- https://gwsocket.io/

### nginx
we ad a new location in
```
vim /etc/nginx/site-available/example.com.conf
```
```
location /stats {
  alias /var/www/example.com/stats/;
}
 ```
### on the server
```
mkdir /var/www/example.com/stats/
chown -R www-data:www-data /var/www/example.com/stats/
```

### set up systemd

```
vim /etc/systemd/system/goaccess-example-com.service
```
~copy the goaccess conf to local path so each stat conf is different or symlink it if you want global conf~
~`cp /etc/goaccess.conf /var/www/example.conf/stats/goaccess-example-com.conf`~


```
[Unit]
Description=example.com GoAccess real-time web log analysis
After=network.target

[Service]
Type=simple

ExecStart=/usr/bin/goaccess -f /var/log/nginx/example.com.log \
           --log-format=COMBINED --real-time-html --load-from-disk \
           --keep-db-files --db-path=/var/www/example.com/stats/ \
           -o /var/www/example.com/stats/index.html \
           --ws-url=wss://example.com/ws --port 7890  


ExecStop=/bin/kill -9 ${MAINPID}
WorkingDirectory=/tmp

NoNewPrivileges=true
PrivateTmp=true
ProtectHome=read-only
ProtectSystem=strict
SystemCallFilter=~@clock @cpu-emulation @debug @keyring @memlock @module \
                  @mount @obsolete @privileged @reboot @resources @setuid \
                  @swap @raw-io

ReadOnlyPaths=/
ReadWritePaths=/proc/self
ReadWritePaths=/var/www/example.com/stats

PrivateDevices=yes
ProtectKernelModules=yes
Protectsystemunables=yes

[Install]
WantedBy=multi-user.target
```

refresh systemd and update systemd
```
systemctl daemon-reload
```
start the service
```
systemctl start goaccess
```
check
```
systemctl status goaccess
```
if ok then
enable it to start automatically
```
systemctl enable goaccess
```
if not go back to see errors resolv and flush systemdctl beforecontinuing
```
systemctl reset-failed
```

wait a minute or so, then use this to check its status

```
systemctl status goaccess
```


preparing websocket

```
vim /etc/nginx/nginx.conf
```

add in the http block

```
map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
}
```

edit the domain site-available conf adding

in the root level
```
upstream gwsocket {
    server 127.0.0.1:7890;
}
```
and in the `server 443` example.com domain block the next

```
location /ws {
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_pass http://gwsocket;
    proxy_buffering off;
    proxy_read_timeout 7d;
}
```
and in the `server 443` example.com domain block under `location /stats `
```
# update to match your local network, to allow access from your LAN
# comment next two lines if you don't mind privacy
# allow 192.168.1.0/24;
# deny all; # block rest of the world

# if using a report.html if not index.html
try_files $uri/index.html =404;

access_log /var/log/nginx/example.com.goaccess-access.log;
error_log /var/log/nginx/example.com.goaccess-error.log warn;
```
apply the config
```
nginx -t
nginx -s reload
```
