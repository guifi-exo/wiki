<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [installation guides](#installation-guides)
  - [install nextcloud in debian9, apache, mysql](#install-nextcloud-in-debian9-apache-mysql)
  - [install nextcloud in debian10, nginx and postgresql](#install-nextcloud-in-debian10-nginx-and-postgresql)
- [migrate from mysql or sqlite to postgresql](#migrate-from-mysql-or-sqlite-to-postgresql)
- [calendar and reminders](#calendar-and-reminders)
- [collabora online](#collabora-online)
  - [issue related to docker and xfs malfunction](#issue-related-to-docker-and-xfs-malfunction)
  - [install cache system for nextcloud](#install-cache-system-for-nextcloud)
  - [run occ operations](#run-occ-operations)
- [extra references](#extra-references)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

# installation guides

## install nextcloud in debian9, apache, mysql

install apache

    apt-get install apache2 libapache2-mod-php7.0

install mysql

    apt-get install mariadb-server php7.0-mysql

install php

    apt-get install php7.0-gd php7.0-json php7.0-curl php7.0-mbstring

install php extensions

    apt-get install php7.0-intl php7.0-mcrypt php-imagick php7.0-xml php7.0-zip

install apache modules

    a2enmod php7.0 rewrite headers env dir mime

download latest nextcloud compatible with php7.0 (debian9)

    cd /var/www/html
    wget -c https://download.nextcloud.com/server/releases/nextcloud-15.0.14.zip
    unzip -qq nextcloud-*.zip
    chown -R www-data: nextcloud

do apache configuration for nextcloud

    vi /etc/apache2/sites-available/nextcloud.conf

option /nextcloud - use this configuration if you want to access nextcloud through example.com/nextcloud - put in file `/etc/apache2/conf-available/nextcloud.conf`:

```
Alias /nextcloud "/var/www/html/nextcloud/"

<Directory /var/www/html/nextcloud/>
  Options +FollowSymlinks
  AllowOverride All

 <IfModule mod_dav.c>
  Dav off
 </IfModule>

 SetEnv HOME /var/www/html/nextcloud
 SetEnv HTTP_HOME /var/www/html/nextcloud

</Directory>
```

enable config

    a2enconf nextcloud.conf

option nextcloud.example.com - in case you use a subdomain like nextcloud.example.com - put in file `/etc/apache2/sites-available/nextcloud.example.com.conf`:

```
<IfModule mod_ssl.c>
  <VirtualHost _default_:443>
    DocumentRoot /var/www/html/nextcloud
    ServerName nextcloud.example.com

    <Directory /var/www/html/nextcloud/>
      Options +FollowSymlinks
      AllowOverride All

      <IfModule mod_dav.c>
       Dav off
      </IfModule>

      SetEnv HOME /var/www/html/nextcloud
      SetEnv HTTP_HOME /var/www/html/nextcloud
      Satisfy Any

    </Directory>

    <Directory "/var/www/html/nextcloud/data/">
      # just in case if .htaccess gets disabled
      Require all denied
    </Directory>

    <IfModule mod_headers.c>
      Header always set Strict-Transport-Security "max-age=15768000; preload"
    </IfModule>

    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

    # add rewrites suggested by https://docs.nextcloud.com/server/stable/admin_manual/issues/general_troubleshooting.html#service-discovery
    <IfModule mod_rewrite.c>
      RewriteEngine on
      RewriteRule ^/\.well-known/host-meta https://nextcloud.example.com/public.php?service=host-meta [QSA,L]
      RewriteRule ^/\.well-known/host-meta\.json https://nextcloud.example.com/public.php?service=host-meta-json [QSA,L]
      RewriteRule ^/\.well-known/webfinger https://nextcloud.example.com/public.php?service=webfinger [QSA,L]
      RewriteRule ^/\.well-known/carddav https://nextcloud.example.com/remote.php/dav/ [R=301,L]
      RewriteRule ^/\.well-known/caldav https://nextcloud.example.com/remote.php/dav/ [R=301,L]
    </IfModule>
  </VirtualHost>
</IfModule>
```

enable site for apache

    a2ensite nextcloud.example.com.conf

add in `/etc/php/7.0/apache2/php.ini`:

    [opcache]
    opcache.enable=1
    opcache.enable_cli=1
    opcache.interned_strings_buffer=8
    opcache.max_accelerated_files=10000
    opcache.memory_consumption=128
    opcache.save_comments=1
    opcache.revalidate_freq=1

replace `memory_limit` in `/etc/php/7.0/apache2/php.ini` to be:

    memory_limit = 512M

go to `/etc/php/7.2/fpm/pool.d` and uncomment `env[PATH] = /usr/local/bin:/usr/bin:/bin` (thanks https://wiki.turris.cz/doc/en/public/nextcloud_security_setup)

[optional] in case you want to have data in different disk/storage:

    mkdir -p /media/data/nextcloud
    chown -R www-data: /media/data/nextcloud
    cd /var/www/html/nextcloud
    mv data /media/data/nextcloud
    ln -s /media/data/nextcloud/data data

create user, database and choose privileges for nextcloud's case: run `mysql_setpermission` and select option 6

run in browser nextcloud and follow steps to install it

enter nextcloud, go to apps and install: calendar, polls, collabora online

## install nextcloud in debian10, nginx and postgresql

install following dependencies:

    apt install php7.3-fpm php7.3-pgsql postgresql-11 php7.3-gd php7.3-json php7.3-curl php7.3-mbstring php7.3-intl php-imagick php7.3-xml php7.3-zip

prepare postgresql

    su - postgres -c psql
    CREATE USER nextcloud with encrypted password 'nextcloud';
    CREATE DATABASE nextcloud encoding 'UTF8' owner nextcloud;
    exit

prepare location

    cd /var/www/html
    wget -c https://download.nextcloud.com/server/releases/nextcloud-18.0.1.zip
    unzip -qq nextcloud-*.zip
    chown -R www-data: nextcloud

follow [this guide]( https://docs.nextcloud.com/server/table/admin_manual/installation/nginx.html#nextcloud-in-the-webroot-of-nginx), in my case I did the following change on top

```
# thanks https://docs.nextcloud.com/server/stable/admin_manual/installation/nginx.html#nextcloud-in-the-webroot-of-nginx
upstream php-handler {
    #server 127.0.0.1:9000;
    #server unix:/var/run/php/php7.2-fpm.sock;
    #
    # did
    #   php-fpm7.3
    # said
    #   [17-Feb-2020 13:18:14] ERROR: Another FPM instance seems to already listen on /run/php/php7.3-fpm.sock
    server unix:/run/php/php7.3-fpm.sock;
}
```

https://docs.nextcloud.com/server/stable/admin_manual/installation/source_installation.html#php-fpm-tips-label

- A warning says that php is not properly configured. In `/etc/php/7.3/fpm/pool.d/www.conf` uncomment line `env[PATH] = /usr/local/bin:/usr/bin:/bin` . Extra source: https://docs.nextcloud.com/server/stable/admin_manual/installation/source_installation.html#php-fpm-tips-label
- A warning suggests increasing the memory limit for php. In `/etc/php/7.3/fpm/php.ini` replace line `memory_limit = 128M` with `memory_limit = 512M`
- A warning suggests to index something (expensive on large deployments) `su www-data -s /bin/sh -c "php /var/www/html/nextcloud/occ db:add-missing-indices"`
- A warning suggests to convert something (expensive on large deployments) `su www-data -s /bin/sh -c "php /var/www/html/nextcloud/occ db:convert-filecache-bigint"`
- A warning suggests to adapt http headers (add this in nginx server or general nginx.conf) and reload nginx:

after all of that: `systemctl restart php7.3-fpm.service` (maybe restart nginx too)

```
add_header X-Frame-Options SAMEORIGIN;
add_header X-Robots-Tag none;
add_header X-Download-Options noopen;
add_header X-Permitted-Cross-Domain-Policies none;
```

# migrate from mysql or sqlite to postgresql

adapt the following environment to your needs and execute:

    username="nextcloud"
    hostname="localhost"
    dbname="nextcloud"
    dbpass="nextcloud"
    su www-data -s /bin/sh -c "php /var/www/html/nextcloud/occ db:convert-type --all-apps --port 5432 --password $dbpass pgsql $username $hostname $dbname --no-interaction"

after executing that script the database is automatically moved in the nextcloud configuration from dbtype `mysql` to `pgsql`, etc.

extra source https://docs.nextcloud.com/server/stable/admin_manual/configuration_database/db_conversion.html

# calendar and reminders

enable calendar app

activate cronjob: settings / basic settings / background jobs: select Cron

in a terminal, do `crontab -u www-data -e` and add the following line

    */5  *  *  *  * php -f /var/www/html/nextcloud/cron.php

src https://docs.nextcloud.com/server/stable/admin_manual/configuration_server/background_jobs_configuration.html

# collabora online

install docker

```
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

apt-key fingerprint 0EBFCD88

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

apt-get update

apt-get install docker-ce docker-ce-cli containerd.io

after that you have to reboot https://github.com/docker/for-linux/issues/598

source https://docs.docker.com/install/linux/docker-ce/debian/ or

[apache reverse proxy option](https://www.collaboraoffice.com/code/apache-reverse-proxy/)

[nginx reverse proxy option](https://www.collaboraoffice.com/code/nginx-reverse-proxy/)

I had to add this entry, which helped nextcloud to detect the collaboraonline thing (?)

```
    location / {
        proxy_pass https://127.0.0.1:9980;
        proxy_set_header Host $http_host;
    }
```

because we are using spanish and english dictionaries, we have to install them according to [this source](https://github.com/husisusi/officeonlin-install.sh/issues/155#issuecomment-457002079)

apt install hunspell locales-all hunspell-en-us hunspell-es

if your reverse proxy is in localhost (or change 127.0.0.1 to the interface you can securely listen)

this also serves as an script to upgrade docker, change the target_tag and be careful with the space it fills in your system

```
# src https://hub.docker.com/r/collabora/code/tags
# tag: last time checked 2020-7-15
target_tag="4.2.5.3"
nextcloud_domain="nextcloud.example.com"   # or example.com if is a subdomain
# this is not needed
#nextcloud_domain="$(echo $nextcloud_domain | sed 's/\./\\\\\./g')"
docker run -t -d -p 127.0.0.1:9980:9980 -e "domain=$nextcloud_domain" -e 'dictionaries=en es ..' --restart always --cap-add MKNOD collabora/code:$target_tag
```

if you have firewall:

- ACCEPT traffic for docker0 bridge interface
- don't allow docker to modify firewall, put `DOCKER_OPTS="--iptables=false"` in `/etc/default/docker`

a way to validate that collabora online is working:

https://office.example.com/hosting/capabilities

extra links to check that it is working ([thanks](https://www.allerstorfer.at/nextcloud-install-collabora-online/)):

- https://office.example.com/hosting/discovery
- https://office.example.com/loleaflet/dist/admin/admin.html

extra link about how to compile and install yourself (docker contains a limitation of less than 60 persons in the same document) https://github.com/husisusi/officeonlin-install.sh

## issue related to docker and xfs malfunction

problem: xfs with d_type/ftype false is problematic with docker https://docs.docker.com/storage/storagedriver/overlayfs-driver/

workaround: create a new xfs partition

create new directory in that new partition for docker

    mkdir -p /media/data/docker

find docker.service

    systemctl status docker

in order changes to take effect after upgrade, you need an override method (thanks https://success.docker.com/article/using-systemd-to-control-the-docker-daemon), do `systemctl edit docker` and add the location (thanks https://www.rb-associates.co.uk/blog/move-var-lib-docker-to-another-directory/) this way:

    [Service]
    ExecStart=
    ExecStart=/usr/bin/dockerd -g /media/data/docker -H fd:// --containerd=/run/containerd/containerd.sock

reload systemd config

    systemctl daemon-reload

force the usage of overlay2 (src https://docs.docker.com/storage/storagedriver/overlayfs-driver/). Add in `/etc/docker/daemon.json`

```json
{
  "storage-driver": "overlay2"
}
```

## install cache system for nextcloud

install APCu and redis as backend cache system:

    apt install php-apcu redis-server php-redis

[redis is significantly faster as a unix socket](https://guides.wp-bullet.com/how-to-configure-redis-to-use-unix-socket-speed-boost/), in `/etc/redis/redis.conf` put:

    unixsocket /var/run/redis/redis-server.sock
    unixsocketperm 775

restart redis to apply configuration changes

    service redis-server restart

add www-data to redis group and restart apache2 to have proper permissions

    gpasswd -a www-data redis
    service apache2 restart  # or nginx

configure APCU as memcache and redis for file locking, in `/var/www/html/nextcloud/config/config.php` add:

    'memcache.locking' => '\OC\Memcache\Redis',
    'memcache.local' => '\OC\Memcache\Redis',
    'memcache.distributed' => '\OC\Memcache\Redis',
    'redis' => [
         'host'     => '/var/run/redis/redis-server.sock',
         'port'     => 0,
         'dbindex'  => 0,
         'timeout'  => 1.5,
    ],

https://docs.nextcloud.com/server/stable/admin_manual/configuration_server/caching_configuration.html

using redis as memcache.local, suggested here https://help.nextcloud.com/t/solved-apcu-caching-gives-error/9403/5

## run occ operations

check new nextcloud version

su www-data -s /bin/sh -c "php /var/www/html/nextcloud/occ update:check"

noninteractive upgrade

su www-data -s /bin/sh -c "php /var/www/html/nextcloud/updater/updater.phar --no-interaction"

src https://docs.nextcloud.com/server/18/admin_manual/maintenance/update.html#batch-mode-for-command-line-based-updater

# extra references

http://blackhold.nusepas.com/2019/04/02/nextcloud-sobre-nginx-y-postgresql-paquetes-ofimaticos-collabora-o-onlyoffice-ds-sobre-docker/
