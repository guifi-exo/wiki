# sftp with chroot

You need to follow the full guide to get it done, in summary you require ssh configuration and appropriate permissions

scenario: an untrusted user2 wants to do read and write access to /var/www/html/wordpress/wp-content which is owned by user1

Hence, a chroot will be performed in `/var/www/html/wordpress` (the parent directory)

## handson

ssh configuration basically is adding the next text block in `/etc/ssh/sshd_config`

```
Match User user2
        ChrootDirectory /var/www/html/wordpress
        ForceCommand internal-sftp
        AllowAgentForwarding no
        AllowTcpForwarding no
        X11Forwarding no
        # # uncomment next line if you allow password auth for these kind of sftp sessions
        # PasswordAuthentication yes
```

restart ssh service: `service ssh restart`

[**don't run the next 3 commands**] for illustrative purposes, the wordpress hardening is mostly represented by ([check full details](https://gitlab.com/guifi-exo/wiki/blob/master/howto/wordpress.md)):

    chown -R user1:user1 /var/www/html/wordpress
    find /var/www/html/wordpress -type f -exec chmod 0644 {} \;
    find /var/www/html/wordpress -type d -exec chmod 0755 {} \;

and these are the **permissions required to be able to run a chroot** (if you don't do that, you will get a *broken pipe*):

    chown root:www-data /var/www/html/wordpress
    chmod 755 /var/www/html/wordpress

to have write permissions in user2 you need to **give the permissions of group user1 in the user user2**

    gpasswd -a user2 user1

and **give write permissions to group**

    cd /var/www/html/wordpress
    find . -type f -exec chmod 0664 {} \;
    find . -type d -exec chmod 0775 {} \;

## overview - summary - again

to get an idea of everything let's take a look to next commands in a sftp session:

```
 $ sftp user2@myhost
sftp> pwd
Remote working directory: /
sftp> ls -alh
drwxr-xr-x    ? 0        33           4.0K Nov  9 00:24 .
(...)
drwxrwxr-x    ? 1000     1000         133B Nov  9 00:29 wp-content
```

chroot needs to be managed in its root directory with root owner and 755 permissions, hence, the root directory `/` cannot be modified. In the previous sftp session, the `ls` command there is root (represented by 0) as an owner and www-data (represented by 33) as a group, all contents included like `wp-content` are mostly for the wordpress user (represented by 1000). Our new user requires the permissions of that user to modify files (hence: 664 permissions for files and 775 for directories).

## notes and references

- general extra note: the way `find` command is used does not follow symlinks (and that's what we want)

- important reference: https://wiki.archlinux.org/index.php/SFTP_chroot#Create_an_unprivileged_user

- obscure setuid related security risks outlined here: http://lists.mindrot.org/pipermail/openssh-unix-dev/2009-May/027651.html (https://stackoverflow.com/questions/18220104/write-failed-broken-pipe-when-trying-to-login-through-ssh-with-a-specific-use/18246535#18246535)
