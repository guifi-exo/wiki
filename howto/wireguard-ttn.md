# Sistema d'accés a Internet per VPN de TTN-Guifi.net per dispositius OpenWRT

El projecte TTN-guifi.net es basa en la col·laboració de la comunitat TTN a Catalunya i la de Guifi.net per tal de desplegar una xarxa de gateways [Lora](https://en.wikipedia.org/wiki/LPWAN#LoRa) del [LORIX One](https://lorixone.io/) a la xarxa oberta Guifi.net.

## Descripció de la solució

Els gateways LoraWAN estan configurats per tal de paquetitzar en IP i enviar les dades col·lectades pels sensors al servidor `router.eu.thethings.network`.
Donat que en general a la xarxa Guifi.net no existeixen rutes per defecte per accedir a Internet, cal establir algun tipus de túnels cap alguna passarel·la amb accés a Internet. 
Hem triat un sistema basat en Wireguard, un programari que permet l'establiment d'una VPN de manera simple i fàcilment implementable amb OpenWRT. Fonamentalment es tracta
de definir interfícies de xarxa entre la passarel·la o *broker* i els punts extrems o *gwt*. Cal tenir en compte que els propis gateways LoraWAN considerats (del fabricant LorixOne)
no implementen de manera nativa cap tipus de programri de túnel o VPN. Per tant, hem hagut de situar aquests nous elements *gwt* entre els gateways LoraWAN i la xarxa Guifi.net.
Per tal de millorar la flexibilitat en l'escalabilitat de la solució, hem incorporat un protocol d'encaminament dinàmic: OLSR. La topologia general del sistema es
pot resumir amb el següent esquema:

```
                                   Guifi.net
                                    +-----+         +-----------+
+--------+      +------------+      |     |         |           |
|        |      |            +----------------------+ TTN Gwt-1 +---//GWLora1\\
| FXOLN  +------+ TTN Broker |      |     |         |           |
|        |      |            +--------------+       +-----------+
+--------+      +------------+      |     | |       +-----------+
                                    +-----+ |       |           |
                                            +-------+ TTN Gwt-2 +---//GWLora2\\
                                                    |           |
                                                    +-----------+
```

Cal tenir una sèrie de consideracions per tal que aquest plantejament funcioni correctament:

1. OLSR fa servir trànsit broadcast sobre les interfícies orientades a routing, és a dir, les que connecten amb els nodes adjacents
2. Wireguard en aquests moments, no permet la propagació de trànsit broadcast o multicast en el cas d'interfícies wireguard amb més de dos endpoints
3. Cada interfície diferenciada de Wireguard necessita un port UDP diferent, tot i que és possible reusar les claus privades/públiques de cada peer

Com a conseqüència cal configurar en el *broker* tantes interfícies Wireguard com *gwt* existeixin i en cada una cal assignar una adreça IPv4 de backbone. El pla d'adreçament considerant una solució tipus VPN amb protocol d'encaminament intern:

| Segment       | Subxarxa IPv4| Hosts adreçables | Comentaris  |Prefix d'agregació|
| ------------- |-------------:| -----:|--------:|--------:|
| GWs Lora Grup 0 | `172.16.N.0/24`| `254` | `N=1,2,...,255`|`172.16.0.0/14`|
| GWs Lora Grup 1 | `172.17.N.0/24`| `254` | `N=1,2,...,255`|`172.16.0.0/14`|
| GWs Lora Grup 2 | `172.18.N.0/24`| `254` | `N=1,2,...,255`|`172.16.0.0/14`|
| GWs Lora Grup 3 | `172.19.N.0/24`| `254` | `N=1,2,...,255`|`172.16.0.0/14`|
| Backbone Grup 0 Broker->GW-N| `172.31.N.1/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|
| Backbone Grup 0 GW-N->Broker| `172.31.N.254/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|
| Backbone Grup 1 Broker->GW-N| `172.30.N.1/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|
| Backbone Grup 1 GW-N->Broker| `172.30.N.254/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|
| Backbone Grup 2 Broker->GW-N| `172.29.N.1/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|
| Backbone Grup 2 GW-N->Broker| `172.29.N.254/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|
| Backbone Grup 3 Broker->GW-N| `172.28.N.1/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|
| Backbone Grup 3 GW-N->Broker| `172.28.N.254/24`| `254`|`N=1,2,...,255`|`172.31.0.0/21`|

## Preparatius del builroot d'OpenWRT

El maquinari que hem triat és [Air Gateway](https://www.ubnt.com/accessories/airgateway/) de Ubiquiti Networks i [Nexx WT3020](https://openwrt.org/toh/nexx/wt3020). Aquest dispositiu és molt compacte, resol els problemes de PoE i integra un CPE totalment compatible amb OpenWRT.
En aquest document farem ús del buildroot d'OpenWRT però també es poden generar els binaris amb el SDK. Preparem l'entorn de desenvolupament:

Clonem els arxius font:
```
# git clone https://git.lede-project.org/source.git lede
```
Tot seguit triem la versió estable a través del tag. Podem llistar els tags disponibles així:
```
# git tag --list
reboot
v17.01.0
v17.01.0-rc1
v17.01.0-rc2
v17.01.1
v17.01.2
v17.01.3
v17.01.4
v17.01.5
v17.01.6
v18.06.0
v18.06.0-rc1
v18.06.0-rc2
v18.06.1

# git checkout v18.06.1
```
Ara creem les referències explícites de tots els paquets:
```
# ./scripts/feeds update -a
# ./scripts/feeds install -a
```

Ara cal triar els paquets que cal compilar. Això es pot fer a través de `make menuconfig`. Podeu compilar les imatges incorporant totes les configuracions amb el repositori [ttncat-config.git](https://gitlab.com/guifi-exo/ttncat-config). El que fem és triar l'aquitectura que fem servir i les altres opcions i paquets. Per tal d'incloure les configuracions correctes en el binari compilar cal establir un enllaç simbòlic amb el directori on es troben les fonts. En el nostres cas:
```
# ln -s ../files_air_ttn files
```

Un cop aplicada la confguració amb l'arxiu `.config` executem el procés de compilació:
```
make
```
si tot va bé, trobarem les imatges a `<build_dir>/bin/targets/ar71xx/generic/`.

## Configuracions de referència
En primer lloc cal generar la parella de claus pública i privada wireguard. Podem generar-les i desar-les en els arxius `public_key` i `private_key` en el mateix router:
```
root@GWT-X:~# wg genkey | tee private_key | wg pubkey > public_key
```

La configuració de referència pel *TTN gwt* en el cas que aquest tingui una IP nadiu de Guifi.net és (les claus privades són fictícies):
```
# /etc/config/network

config interface 'loopback'
        option ifname 'lo'
        option proto 'static'
        option ipaddr '127.0.0.1'
        option netmask '255.0.0.0'

config globals 'globals'

config interface 'lan'
        option type 'bridge'
        option proto 'static'
        option netmask '255.255.255.0'
        option ip6assign '60'
        option _orig_ifname 'eth1 wlan0'
        option _orig_bridge 'true'
        option ifname 'eth1'
        option ipaddr '172.16.1.1'
        option dns '8.8.8.8'

config interface 'wan'
        option ifname 'eth0'
        option _orig_ifname 'eth0'
        option _orig_bridge 'false'
        option proto 'static'
        option ipaddr '10.228.201.201'
        option netmask '255.255.255.240'

config interface 'ttn'
        option proto 'wireguard'
        option private_key 'mLVrzXXcGkBvE0nYxPTs/Ho1234567s9AiXjtMlQovkI='
        # Permetem que el port origen sigui aleatori
        # option listen_port '45955'
        list addresses '172.31.1.254/24'
        option mtu '1350'

config wireguard_ttn
        list allowed_ips '0.0.0.0/0'
        option endpoint_host '10.38.140.235'
        option public_key 'nbx75FpQLUmVV4s/YHSnzdLOfmP46bmp1qD06cRSQwo='
        option endpoint_port '45955'

config route
        option interface 'wan'
        option target '10.0.0.0'
        option netmask '255.0.0.0'
        option gateway '10.228.201.193'
```
En el cas que el *TTN gwt* estigui darrera d'un NAT, cal eliminar l'opció de port d'entrada fix:

```
config interface 'ttn'
        option proto 'wireguard'
        option private_key 'mLVrzXXcGkBvE0nYxPTs/Ho1234567s9AiXjtMlQovkI='
        #option listen_port '45955'
        list addresses '172.31.1.254/24'
        option mtu '1350'
```
Pel que fa al *TTN broker* tenim (les claus privades són fictícies):
```
# /etc/config/network

config interface 'loopback'
        option ifname 'lo'
        option proto 'static'
        option ipaddr '127.0.0.1'
        option netmask '255.0.0.0'

config globals 'globals'

config interface 'lan'
        option type 'bridge'
        option ifname 'eth0'
        option proto 'static'
        option ipaddr '10.38.140.235'
        option netmask '255.255.255.224'
        option ip6assign '60'
        option dns '8.8.8.8'
        option gateway '10.38.140.225'

config interface 'wan'
        option proto 'static'
        option ifname 'eth1'
        option ipaddr '109.69.10.98'
        option netmask '255.255.255.224'
        option gateway '109.69.10.126'

config route
        option interface 'lan'
        option target '10.0.0.0'
        option netmask '255.0.0.0'
        option gateway '10.38.140.225'

config interface 'ttn_1'
        option proto 'wireguard'
        option private_key 'SG5RemRhqTPF6g12345675G/KQbkOfY4COT4taIjA3w='
        option listen_port '45955'
        list addresses '172.31.1.1/24'
        option mtu '1350'

config wireguard_ttn_1
        option public_key '7JfICIH5zKTSoH/5YT8kMkDmVQdg5Oy2r4PM2PId81c='
        # Acceptem qualsevol port origen
        #option endpoint_port '45955'
        list allowed_ips '0.0.0.0/0'

config interface 'ttn_2'
        option proto 'wireguard'
        option listen_port '45956'
        list addresses '172.31.2.1/24'
        option private_key 'SG5RemRhqTPF6g12345675G/KQbkOfY4COT4taIjA3w='
        option mtu '1350'

config wireguard_ttn_2
        option public_key 'ArDlCloA0yqzEqxeTUlVZfq1NaODpBoQn8mQ6TTwukk='
        option endpoint_port '45956'
        list allowed_ips '0.0.0.0/0'
```

La configuració per tal que funcioni l'anunci de prefixes dins la VPN a la part *TTN broker* és:
```
# /etc/config/olsrd

config olsrd
        option IpVersion '4'
        option FIBMetric 'flat'
        option LinkQualityLevel '2'
        option LinkQualityAlgorithm 'etx_ff'
        option OlsrPort '698'
        option Willingness '3'
        option NatThreshold '1.0'
        option MainIp '0.0.0.0'

config LoadPlugin
        option library 'olsrd_arprefresh.so.0.1'
        option ignore '1'

config LoadPlugin
        option library 'olsrd_dyn_gw.so.0.5'
        option ignore '1'

config LoadPlugin
        option library 'olsrd_httpinfo.so.0.1'
        option port '1978'
        list Net '0.0.0.0 0.0.0.0'
        option ignore '1'

config LoadPlugin
        option library 'olsrd_nameservice.so.0.3'
        option ignore '1'

config LoadPlugin
        option library 'olsrd_txtinfo.so.0.1'
        option accept '0.0.0.0'
        option ignore '1'

config Interface
        option Mode 'ether'
        option ignore '0'
        option interface 'ttn_1'
        option Ip4Broadcast '172.31.1.255'

config Interface
        option Mode 'ether'
        option ignore '0'
        option interface 'ttn_2'
        option Ip4Broadcast '172.31.2.255'

config InterfaceDefaults
        option Mode 'mesh'
        option Ip4Broadcast '0.0.0.0'

config Hna4
        option netaddr '0.0.0.0'
        option netmask '0.0.0.0'

config LoadPlugin
        option library 'olsrd_jsoninfo.so.0.0'
        option ignore '0'
```

També cal adaptar la configuració per defecte del firewall de OpenWRT. Hem d'afegir aquestes línies a l'arxiu per defecte:
```
# /etc/config/firewall

config zone             
        option input 'ACCEPT'       
        option output 'ACCEPT'  
        option name 'vpn'       
        option forward 'ACCEPT'
        option mtu_fix '1'             
        option network 'ttn_1 ttn_2'      
                              
config forwarding      
        option dest 'lan'  
        option src 'vpn'             
                                   
config forwarding                               
        option dest 'wan'              
        option src 'vpn'              
                                   
config forwarding                           
        option dest 'vpn'         
        option src 'lan'
```

## Generació de claus Wireguard

El paquet wireguard inclou l'eina `wg` que permet generar les claus públiques i privades:

```
# wg genkey > privatekey
# wg pubkey < privatekey > publickey
```

## Descripció d'emplaçaments específics

A continuació hi figuren descripcions de la xarxa Guifi/TTN d'emplaçaments especials.
### Fablab

Aquest emplaçament incorpora una modalitat de connexió no habitual. Compta amb un node mesh de la xarxa de Poblenou i simultàniament un túnel comunitari am el supernode de l'eXO a ITConic. D'aquesta manera és possible encamicar el trànist a Internet dels GWLora per dos camins redundats.
Incorpora un switch gestionable amb la següent configuració:

| VLANID     | Tagged port | Untagged ports | VLAN name |
| :--------- |:------------|:---------------|----------:|
| 1          | 1           | 2-7            | Guifi.net |
| 3          | 1           | 8              | Fablab    |

Tot seguit l'esquema general del dispositius presents en aquest emplaçament:

```
                                                             Patch       Guifi/TTN
                              Fablab/Adamo   Switch TTN      Panel       Equipment
                                 +-----+    +-----------+    +---+     +-----------+
+--------+    +------------+     |     |    |           |    |   |     |           |           /////////////
|        |    |            +<----+     +--->+P8       P1+----+P38+-----+ Node mesh +<<<<<>>>>>>/ Mesh P9SF /
| FXOLN  +----+ TTN Broker |     |     |    |           |    |   |     |           |           /////////////
|        |    |            |     |     |    |           |    |   |     +-----------+                  ^
+--------+    +-----+------+     +-----+    |           |    |   |     +-----------+                  |
                    ^                       |           |    |   |     |           |                  |
                    |                       |         P2+--------------+ TTN Gwt-3 |                  |
                    |                       |           |    |   |     |           |   //GWLora3\\    |
                    |                       |           |  +-+P39+-----+           |         ^        |
                    |                       +-----------+  | |   |     +-----------+         |        |
                    |                                      | +---+                           |        |
                    |                                      +---------------------------------+        |
                    |                                                                                 |
                    +---------------------------------------------------------------------------------+
```