#+TITLE: taller de quick Mesh project (qMp.cat)
#+AUTHOR: guifi.net community

* Introducció
Material bàsic per desplegar xarxes qMp

Els dispositius acostumen a utilitzar Power over Ethernet (PoE) de 24 V, això vol dir que la potència elèctrica i les dades del dispositiu van a través del cable ethernet. El cable blau en la figura [[fig:poe]] es el que s'està desplegant cap a un lloc exterior (normalment una terrassa o un teulat) on els dispositius qMp seran instalats i prendran l'interfície PoE de l'injector PoE. L'altre interfície en l'injector PoE és la LAN [1] i pot emplaçar-se cap a un PC o un switch/router si es vol connectar més d'un PC. Finalment, l'injector PoE requereix alimentació elèctrica estàndard. *Alerta*: una connexió PoE cap a un dispositiu que no estigui preparat per treballar en PoE 24 V, per exemple una interfície de xarxa d'un ordinador, pot causar averia de la interfície de xarxa.

#+CAPTION: Diagrama de xarxa amb PoE
#+NAME: fig:poe
[[./img/general/poe.jpg]]

* Elements bàsics d'instal·lador: Components
- Cable d'exteriors i connectors RJ45 (es venen per separat). El cable hauria de ser d'exteriors per resistir el clima.
  - Cable UTP: cable sense protecció. Però a vegades és suficient pels nostres propòsits.
  - Cable FTP (recomanat): cable amb protecció bàsica. Preveu descàrrega electrostàtica [2]. La diferència és que el cable no té terra.
- Eina de crimpatge: per unir el connector RJ45 amb el cable
- Comprovador (tester) de cable (recomanat): un de baix cost ens assegura un correcte crimpat del cable.
- Tallador de cable: Per agafar un segment de cable per la instal·lació. Nota: El cable UTP es més fàcil de tallar que el FTP
- Brida: Per fixar el cable i el dispositiu al màstil o amb altres cables.
* Elements bàsics d'instal·lador: Dispositius 5 GHz
Els dispositius seleccionats treballen a la banda de 5 GHz perquè 2.4 GHz té un ús molt estès en la població i té moltes interferències. Són del fabricant Ubiquiti i són compatibles amb el firmware qMp.

 - Baix cost :: al voltant dels 70 euro.

   - Nanostation Loco M5 (NSLM5) :: Petites distàncies (menys d'1 km). La connexió amb un altre node candidat és acceptable i no cal incrementar la potència de senyal de wifi. Utilitza el mateix firmware que la NanostationM5.
   - Nanostation M5 (NSM5) :: [3] si és necessari una millor connexió amb el node específic. Recomenada per compartir Internet.
   - NanoBeam M5 (NBM5) diferents models :: [4] Quan hi ha una connexió de llarga distància (més de 1 km).
   - sèrie XW :: Són les noves versions dels dispositius NSLM5, NSM5, etc. però amb processadors nous. El primer introduït va ser NBM5 reemplaçant a NanoBridge M5 (en procés de descatalogar-se). Aquest dispositiu requereix qMp 4 i no és estable encara. La resta de dispositius especificats són AirMax (XM) i s'utilitzen a qMp 3.1 (versió estable).

 - Cost alt :: entre 100 i 300 euro.

   - Rocket M5 :: [5] Estació base per posar diferents tipus d'aparells.
     - Rocket M5 + Antena Sectorial (S) de 90 o 120 graus :: [6] quan la necessitat és cobrir una zona de la regió amb cobertura constant de 90 o 120 graus des de l'aparell. Hi han versions d'alta potència (high gain, HG) i de potència mitja (Mid Gain, MG).
     - Rocket M5 + Plat (Dish, D) :: [7] Distàncies més llargues (Enllaç de 50 km [8]).

Resum d'informació rellevant a la Taula [[tab:devspec]]:
#+CAPTION: Especificacions dels dispositius
#+NAME: tab:devspec
| Devices         | Gain (dBi)     | Beamwidth (deg)     | Proc. | RAM  | Flash |
|                 |                | Hpol/Vpol/Elevation |       | (MB) |  (MB) |
|-----------------+----------------+---------------------+-------+------+-------|
| NSLM5           | 13             | 45/45/45            | 24KC  | 32 S |     8 |
| NSM5            | 16             | 43/41/15            | 24KC  | 32 S |     8 |
| NBM5            | 16, 19, 22, 25 | veure en PDF    | 74KC  | 64 D |     8 |
| XW series       | -              | -                   | 74KC  | -    | -     |
| RM5 S90 MG, HG  | 17, 20         | veure en PDF    | 24KC  | 64 S |     8 |
| RM5 S120 MG, HG | 16, 19         | veure en PDF    | 24KC  | 64 S |     8 |
| RM5 D           | 30, 34         | veure en PDF    | 24KC  | 64 S |     8 |

- Proc :: Especificacions de processador.
  - 24KC :: Atheros MIPS 24KC, 400MHz
  - 74KC :: Atheros MIPS 74KC, 560MHz
- RAM :: Tipus de RAM:
  - S :: SDRAM
  - D :: DDR2
* Operacions bàsiques a qMp: Operacions de testeig
La figura [[fig:wan-status-on]] mostra la primera pantalla obtinguda quan es fa log in a un node qMp

#+CAPTION: Primera pantalla
#+NAME: fig:wan-status-on
[[./img/qMp-basics-scrot/status-wan_status.png]]

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per tornar a aquesta pantalla, aneu al menu i cliqueu a:
: qMp/Mesh / Status
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/status

Al baixar per la pantalla, apareixen les estacions associades (Associated Stations). La figura [[fig:associated-stations]] té els enllaços de wifi amb altres nodes qMp i amb quina senyal s'associen (en dBm). Les bones pràctiques de guifi.net diuen que un enllaç troncal  ha de ser millor que -75dBm [9]. En aquesta figura hi ha diferents tipus de qualitat d'enllaç. Bona qualitat significa alts paràmetres de: dBm, RX Rate, TX Rate [ample de banda (Mbps)] i codificació MCS (el número).

Aquestes qualitats es refereixen a la connexió amb diferents nodes, només mostra la direcció MAC. Però amb la MAC es suficient per identificar el node, ja que els darrers quatre caràcters de la MAC es posen al final del nom del node. Més endavant, es mostrarà com anar a altres nodes de la xarxa.

#+CAPTION: Estations associades
#+NAME: fig:associated-stations
[[./img/qMp-basics-scrot/status-associated-nodes.png]]

Un altre mesura de qualitat es mostrada a la Figura [[fig:links-node]]. Aquesta és la qualitat en termes del protocol d'enrutament de la xarxa (bmx6) amb una escala de 0-100 per la recepció i la transmissió (rx/tx).

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: qMp/Mesh / Mesh / Links
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/mesh/links

#+CAPTION: Enllaços del node
#+NAME: fig:links-node
[[./img/qMp-basics-scrot/links.png]]

També, es pot fer el test de rendiment en  ample de banda entre els nodes. La figura [[fig:bw-test]] executa una prova del rendiment de la connexió i dona els Mbps entre el node des d'on som i altres possibles destinacions. Aplica els test d'un en un per saber l'ample de banda d

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: qMp/Mesh / Tools
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/tools

#+CAPTION: Test de rendiment en ample de banda
#+NAME: fig:bw-test
[[./img/qMp-basics-scrot/test-bandwidth.png]]

Figura [[fig:wifi-signal-rt]]. Després de l'scan general, quan hi ha un node candidat per fer una connexió estable, hi ha la necessitat d'analitzar la qualitat de l'enllaç en temps real. Això ajuda a seleccionar un lloc òptim per fixar el dispositiu en la instal·lació.

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: OpenWRT / Status / Realtime Graphs / Wireless
alternativament:\\ http://admin.qmp/cgi-bin/luci/admin/status/realtime/wireless

#+CAPTION: Guany del millor senyal wifi rebut en temps real
#+NAME: fig:wifi-signal-rt
[[./img/qMp-basics-scrot/realtime_wifi_link.png]]

La situació pot ser que no pot haver connexió del node a la xarxa. Potser la xarxa està a un altre canal. La figura [[fig:find-qmp]] mostra un scan del wifi. qMp sempre utilitza el BSSID: =02:CA:FF:EE:BA:BE=, en el Mode =Ad-Hoc=. Aquestes són dos sòlides referències per trobar altres xarxes qMp. En la figura hi han dos xarxes qMp en canals: 140 i 132.

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: OpenWRT / Network / Wifi / "Scan"
alternativament:\\ http://admin.qmp/cgi-bin/luci/admin/network/wireless and click Scan.

#+CAPTION: Scan de Wifi: trobar xarxa qMp
#+NAME: fig:find-qmp
[[./img/qMp-basics-scrot/wifi_scan_find_qmp.png]]

Si s'està dissenyant una nova xarxa qMp és important escollir un canal que no estigui en ús. La figura [[fig:interference]] mostra com un altre Punt d'accés (Acces Point, AP) està utilitzant el canal 140.

#+CAPTION: Scand de wifi amb interferències
#+NAME: fig:interference
[[./img/qMp-basics-scrot/wifi_scan_interference.png]]

La figura [[wifi-channel-power]] mostra on canviar els paràmetres del wifi, per exemple, canal i la potència del senyal wifi. Per defecte, qMp utilitza potència de senyal 17, però pot ser incrementada.

Utilitza la potència del senyal wifi amb compte, en la xarxa d'interès és una senyal de comunicació i/o cobertura, però per altres xarxes és un altre soroll en l'entorn que fa les comunicacions més difícils. 

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: OpenWRT / Node configuration / Wireless Settings
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/configuration/wifi/

#+CAPTION: Wifi: Canal i potència de senyal
#+NAME: fig:wifi-channel-power
[[./img/qMp-basics-scrot/wifi-channel-power.png]]

La figura [[fig:tunnels]], marcat com vermell, mostra que hi ha un node WAN, el node fa un  anunci de la seva xarxa com =Internet=. Si pot arribar allà, vol dir que hi ha connexió a internet, prova des d'un navegador web. També pot ser interessant executar diferents test de velocitat d'internet [10] [11] [12] [13].

Però potser no es pot arribar al node WAN, o no hi ha node WAN a la xarxa. Es pot comprovar si hi ha un túnel a Internet.

En la mateixa vista, es pot buscar per node Frontera. La figura [[fig:tunnels]] mostra, marcat en blau, com el node fa un anunci de xarxa en =10.0.0.0/8=, això vol dir, accés a la resta de guifi.net

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: qMp/Mesh / Mesh / Tunnels
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/Mesh/Tunnels

#+CAPTION: Túnels
#+NAME: fig:tunnels
[[./img/qMp-basics-scrot/tunnels.png]]

* Operacions bàsiques a qMp: Instal·lació bàsica i manteniment
Figura [[fig:quick-setup]], aquesta és la configuració final del node quan està preparat per estar en la fase "en proves", és a dir, s'espera un funcionament correcte però s'ha connectat recentment.

En la pàgina web de guifi.net, després d'afegir els dispositius, es rep una IPv4 única dins de guifi.net, i és necessari una màscara del tipus =255.255.255.244=. Utilitza el mateix nom de node que en la pàgina web o la web d'organització de la xarxa.

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: qMp/Mesh / Node configuration / qMp easy setup
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/configuration/easy_setup/

#+CAPTION: Configuració ràpida del dispositiu
#+NAME: fig:quick-setup
[[./img/qMp-basics-scrot/quick_setup.png]]

Figura [[fig:backup]]: Quan el node està funcionant bé és important fer una còpia de seguretat de la configuració. Amb qMp, no està recomanat actualitzar el node utilitzant aquest menu.

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: OpenWRT / System / "Backup/Flash Firmware"
alternativament:\\ http://admin.qmp/cgi-bin/luci/admin/system/flashops

#+CAPTION: Còpia de seguretat
#+NAME: fig:backup
[[./img/qMp-basics-scrot/backup-new-firmware.png]]

Per actualitzar el node de moment només és possible a través de la línia de comandes (Command Line Interface, CLI) . Fes un login en la sessió ssh:
: ssh root@admin.qmp
password: 13f \\ A partir d'aquest punt hi ha tres mètodes:
1. Actualització automàtica (amb connexió a internet des del node).
   : qmpcontrol upgrade
2. Actualització amb un enllaç (amb connexió a internet des del node).
   : qmpcontrol upgrade "http://...qmp.bin"
   És a dir la URL on està localitzat el binari de qMp, recorda que es poden trobar tots els binaris suportats en: http://fw.qmp.cat
3. Actualitzar utilitzant un fitxer local (sense connexió a internet des del node)
   1. Posa el fitxer dins del node qMp, obre una línea de comandes i posa
      : scp qmp.bin root@admin.qmp:/tmp
      Et preguntarà per la contrassenya
   2. Amb la connexió ssh existent oberta anteriorment, o amb una nova,
      login per ssh i:
      : qmpcontrol upgrade "/tmp/qmp.bin"
Confirma i continua amb el procés d'actualització i espera fins que finalitzi.

Nota: qMp només guarda configuracions comunes després de l'actualització, concretament:
: # cat /etc/config/qmp | grep preserve

Per altres fitxers canviats, fes un backup abans d'actualitzar
* Operacions bàsiques a qMp: Visualitza la xarxa
La figura [[fig:net-nodes]] mostra una pantalla on són tots els nodes qMp que conformen la xarxa. Fent click a l'icona esfera blava a la dreta de cada node és possible 

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: qMp/Mesh / Mesh / Nodes
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/mesh/nodes

#+CAPTION: Adreces IP dels nodes
#+NAME: fig:net-nodes
[[./img/qMp-basics-scrot/net-of-nodes.png]]

La figura [[fig:graph-network]] és un graf que mostra els nodes, les arestes amb la qualitat de bmx6 mostra com estan connectats entre ells.

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: qMp/Mesh / Mesh / Graph
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/mesh/graph

#+CAPTION: Graf de la xarxa
#+NAME: fig:graph-network
[[./img/qMp-basics-scrot/graph.png]]
* Propostes de disseny d'una xarxa qMp: disseny del node WAN
Per construir el node WAN, la figura [[fig:wan-gen]] mostra com un node qMp s'hauria de connectar a la xarxa (a través del wifi amb el protocol d'enrutament bmx6) i d'Internet (a través de l'ethernet cap al router de l'ISO [14] amb el DHCP client).

Es recomana utilitzar el dispositiu Nanostation M5 ja que té dos interfícies de xarxa (eth0, eth1). Amb una es pot fer un DHCP server per connectar a un node qMp des d'un portàtil. I amb l'altre ethernet, un DHCP client per connectar al router de l'ISP.

Si és una Nanostation Loco M5, només té una ethernet (eth0 [15]). Aquesta interfície serà utilitzada per la connexió a l'accés a internet a través del ISP router (DHCP client). Això vol dir que no hi ha forma automàtica de connectar-se al node qMp des del portàtil (no hi ha DHCP server). Per accedir a aquest node, utilitza un altre node qMp que estigui en la mateixa xarxa (utilitzant la interfície wifi).

#+CAPTION: Diagrama de xarxa genèric amb el WAN node
#+NAME: fig:wan-gen
[[./img/mesh-designs/wan_node_generic.png]]

Per establir la ethernet que farà de DHCP client cap al router ISP hi ha 2 opcions.

Opció 1: en el quick setup, l'última part parla de què fer amb les interfícies (figura [[fig:quickdhcp]]). Les interfícies tenen 3 seleccions: =Mesh=, =Lan= (DHCP server) i =WAN= (DHCP client).

#+CAPTION: Opció 1: Establir el DHCP client a una interfície amb el quick setup
#+NAME: fig:quickdhcp
[[./img/qMp-basics-scrot/quick_setup_interfaces.png]]

Opció 2: La figura [[fig:netset]] mostra la pantalla per establir la interfície de DHCP client, i no hi ha necessitat de fer un quick setup en el node.

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: OpenWRT / Node configuration / Network Settings
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/configuration/network/

#+CAPTION: Opció 2: Establir a la interfície el DHCP client amb el menu network settings
#+NAME: fig:netset
[[./img/qMp-basics-scrot/network_settings.png]]

Per provar que està funcionant el DHCP client cap al router ISP, comprova el IPv4 WAN Status, secció Xarxa. La figura [[fig:wan-status-on-detail]] mostra una exitosa connexió WAN. La figura [[fig:wan-status-off]] mostra que no hi ha connexió WAN: no hi ha DHCP client o no està correctament connectat.

#+CAPTION: WAN status online
#+NAME: fig:wan-status-on-detail
[[./img/qMp-basics-scrot/status-wan_status_detail.png]]

#+CAPTION: WAN status offline
#+NAME: fig:wan-status-off
[[./img/qMp-basics-scrot/wan_not_connected.png]]

#+BEGIN_LATEX
\noindent
#+END_LATEX
Per arribar a allà, aneu al menú clicant a:
: qMp/Mesh / Mesh / Status
alternativament:\\ http://admin.qmp/cgi-bin/luci/qmp/status
* Propostes de disseny d'una xarxa qMp: disseny general d'un node
La figura [[fig:gen-node]] mostra els elements de la instal·lació d'un node senzill: un node qMp connectat a la xarxa i un router wifi en 2.4 GHz com AP per donar cobertura  dins d'un espai.
#+CAPTION: Diagrama de xarxa d'un node genèric
#+NAME: fig:gen-node
[[./img/mesh-designs/generic_node.png]]
* Flash a un node qMp
Passos per /flashejar/ o posar el sistema qMp en el dispositiu. S'assumeix que es fa des d'un ordinador Ubuntu/Debian GNU/Linux i una antena de 5 GHz recomanada prèviament:
1. Descarrega la imatge *Factory* [16] per un dispositiu suportat que té el sistema del fabricant [17]. La imatge *Sysupgrade* és per nodes on hi ha OpenWRT o qMp i que es volen actualitzar. La imatge *Guifi* té millor integració amb la web de guifi.net.
2. Reanomena el fitxer descarregat per =qmp.bin=
3. Descarrega els packets tftp des del teu repositori del sistema. Des de la CLI: =$sudo apt-get install tftp-hpa=.
4. Desconnecta la connexió a internet.
5. Obre una CLI i escriu:
   : $ ping 192.168.1.20
   T'ajudarà saber quan el dispositiu està en el mode reset.
6. Connecta els equipaments com es mostra a la Figura [[fig:flashdiagram]].
   #+CAPTION: Diagrama de xarxa per flashejar un dispositiu
   #+NAME: fig:flashdiagram
   [[./img/general/flashdiagram.jpg]]
7. Configura la xarxa amb una de les següents opcions:
   1. *Opció GUI*: configura amb el teu administrador de xarxa preferit una ethernet amb IP estàtica en l'ordinador per connectar-lo al dispositiu: \\ IP: 192.168.1.10 \\ Mascara de xarxa (Subnet): 192.168.1.100 \\ Porta d'enllaç (Gateway): 192.168.1.1
   2. *Opció CLI*: 
      : $ sudo ip a a 192.168.1.25/24 dev eth0
8. Posa el dispositiu a mode TFTP server amb una d'aquestes opcions:
   1. *Reset des del dispositiu*: Desconnecta la interfície del dispositiu. Treu la tapa del dispositiu. Amb una mà agafa un objecte amb punta rodona i apreta de forma continuada el botó de reset (Figura [[fig:resetant]]). Mentrestant amb l'altre mà inserta el cable ethernet a l'interfície del dispositiu
      #+CAPTION: Reset device
      #+NAME: fig:resetant
      [[./img/general/reset-device.jpg]]
   2. *Reset des de l'injector PoE*: Comprova que el dispositiu va amb reset al PoE (Figura [[fig:resetpoe]]). Desconnecta la interfície PoE de l'injector PoE. Amb una mà agafa un objecte amb punta rodona i apreta de forma continuada el botó de reset. Mentrestant amb l'altre mà inserta el cable ethernet a l'interfície PoE de l'injector PoE.
      #+CAPTION: Botó reset a l'injector PoE
      #+NAME: fig:resetpoe
      [[./img/general/reset-injector.jpg]]
9. Observa si el dispositiu comença el mode reset per continuar:
   - *Opció leds en el dispositiu*: Espera mentre el led 1 i 3 canvien fins a 2 i 4 cíclicament. Amb aquest recurs de vídeo et faràs una idea del temps i els colors dels leds involucrats al procés [18].
   - *Opció pantalla de l'ordinador*: el ping començarà a respondre. La sortida de =ping 192.168.1.20= hauria de ser similar a:
    : 64 bytes from X: icmp_req=X ttl=X time=X ms
10. Si està en mode reset deixa d'apretar el botó de reset i posa el dispositiu en una zona estable.
11. Obre una finestra de CLI, ves on està descarregat el firmware (sistema qMp) =qmp.bin=.
    : cd /lloc/on/es/el/directori_qmpbin
    I després, executa:
    : $ tftp 192.168.1.20
    : $ mode octet
    : $ trace
    : $ put qmp.bin
    [ Procés de transmissió ]
    : $ quit
12. Després de 5 minuts, el 4rt led de la rampa (el de més a la dreta, figura [[fig:ledsdevice]]) està encès, no parpadejant. Aquest és el moment per anar al proper pas.
    #+CAPTION: Sistema Led del dispositiu
    #+NAME: fig:ledsdevice
    [[./img/general/blinkingled.jpeg]]
13. Reconfigura la xarxa per fer un DHCP client en un port ethernet (IP Automàtica) i prova de connectar de nou l'ordinador al dispositiu.
14. Comprova que el dispositiu respon al ping:
    : $ ping 172.30.22.1
    Aquesta és l'adreça fixa en el mode roaming (per defecte a qMp). \\ Una forma més general és aconseguir la porta d'enllaç (gateway):
    : $ ip r | grep default | cut -f3 -d' '
    Obre el navegador web i comprova si aquesta web es pot accedir (*Alerta* només funcionarà si el PC s'ha connectat al dispositiu amb DHCP):
    : http://admin.qmp
    alternatives:
    : http://172.30.22.1
    : http://<gateway_ip>
15. El login d'accés és
    usuari: root \\ contrassenya: 13f

Altres referències [19] [20] [21]
* Fent una panoràmica amb Hugin
Amb el programa Hugin és molt fàcil fer fotos panoràmiques, i és software lliure [22]. Les panoràmiques són molt útils per saber visualment quins són els nodes propers.

1. Com fer les fotos? Manté la mateixa ubicació física i comença a fer fotos amb un solapament del 20% entre elles.
2. Segueix els passos al programa Hugin (figura: [[fig:hugin]])
   1. =1.Load images=, selecciona totes les imatges el directori amb el qual es vol fer la panoràmica.
   2. =2.Align=.
      - això processa els punts en comú (punts de control) en les diferents fotos per donar sensació de continuïtat.
      - si no hi ha suficients punts de control, busca els punts de control manualment o torna a fer les fotografies.
   3. =3.Create panorama=: guarda la foto en fitxers .pto i .tiff en el directori on són totes  les imatges.
   #+CAPTION: Hugin
   #+NAME: fig:hugin
   [[./img/general/hugin.png]]
3. Conversió de .tiff a .jpeg \\
   Si es vol compartir la panoràmica.
   : sudo apt-get install imagemagick
   : convert pan.tiff pan.jpeg
   Un exemple es mostra a la figura [[fig:exhugin]]
   #+CAPTION: Exemple de panoràmica utilitzant hugin
   #+NAME: fig:exhugin
   [[./img/santandreudeploy/llenguadoc.jpg]]

* Sobre monitorització de la xarxa
Monitoritzar la xarxa és important com a mesura de qualitat. Es presenten 3 alternatives.
** *Des de la web de guifi.net*
es poden obtenir els gràfics. Ajuda a saber si els dispositius està funcionant, la seva latència (ping) i el tràfic de xarxa. La figura [[fig:snpservices]] mostra com es veu.

#+CAPTION: Servidor de gràfiques a guifi.net
#+NAME: fig:snpservices
[[./img/general/snpservices.png]]

Es requereix una versió qMp amb el paquet guifi: hauria d'aparèixer =qMp-Guifi= al fitxer descarregat.

La part del server utilitza el paquet desenvolupat per la comunitat de guifi.net anomenat =snpservices=. Per instal·lar-lo es pot seguir aquesta guia [23], bàsicament, s'obté un repositori de Debian, s'instal·la el paquet i es dona l'identificador (id) del servidor de gràfiques (altres paràmetres per defecte). Per obtenir l'id del servidor de gràfiques s'ha de crear un servei de gràfiques a la web de guifi.net. Per exemple, el id del servidor de gràfiques de Barcelona es pot tenir de la URL: =http://guifi.net/en/node/55045=, i és =55045=.

qMp utilitza el paquet =mini_snmpd= [24] configurat de la web de guifi.net. Després de crear el node i el trasto a la web, es genera el fitxer =unsolclic=. La figura [[fig:qmpquifi]] mostra com de simple és: posar allà la URL del dispositiu i aplicar.

#+CAPTION: menú guifi.net al sistema qMp
#+NAME: fig:qmpguifi
[[./img/qMp-basics-scrot/qmpguifi.png]]
** *munin*:
Per GNU/Linux Debian 7 Wheezy server (apache 2.2)
: sudo apt-get install munin
per defecte fa la monitorització del propi servidor on està funcionant (localhost).

Per posar els gràfics disponibles a tots els usuaris de la comunitat guifi.net [25], seguint el model de tenir accés a les dades de la xarxa, canvia les següent línies a =/etc/munin/apache.conf=:
: Order allow,deny
: Allow from localhost 127.0.0.0/8 ::1
: Options None
de la següent manera:
: Order allow,deny
: Allow from all
: Options FollowSymLinks SymLinksIfOwnerMatch
Apply the changes in the HTTP server:
: # service apache2 restart

Afegeix nodes qMp per monitoritzar editant el fitxer =/etc/munin/munin.conf=:
#+begin_src conf
[qMp-node1] address 10.x.x.x
use_node_name yes
[qMp-node2] address 10.x.x.x
use_node_name yes
#+end_src

Aplica els canvis en el monitor (començaran a aparèixer després de pocs minuts):
: # service munin-node restart
Els gràfics són similars als de la web de guifi, però donen més informació. Excepte que hi ha un error que no permet veure el tràfic de xarxa.
** *qmpsu*:
De moment, no hi ha un paquet genèric de qmpsu per les xarxes qMp, només al barri de Sants (Barcelona). La figura [[fig:qmpsu]] mostra com és.

#+CAPTION: Mostra de qmpsu
#+NAME: fig:qmpsu
[[./img/general/qmpsu.png]]

* Footnotes

[1] Per tenir una referència, el cable LAN pot fer de llarg 100 m només transportant dades 

[2] http://en.wikipedia.org/wiki/Electrostatic_discharge

[3] http://dl.ubnt.com/datasheets/nanostationm/nsm_ds_web.pdf

[4] http://ubnt.com/downloads/datasheets/nanobeam/NanoBeamM_DS.pdf

[5] http://ubnt.com/downloads/rocketM5_DS.pdf

[6] http://dl.ubnt.com/AirMax5GSectors.pdf

[7] http://ubnt.com/downloads/datasheets/rocketdish/rd_ds_web.pdf

[8] http://blog.altermundi.net/article/completamos-el-enlace-de-50km/

[9] http://guifi.net/ca/BonesPractiquesUER

[10] http://www.catnix.net/en/speedtest

[11] http://speedtest.net

[12] http://testdevelocidad.es

[13] http://testvelocidad.eu/

[14] Internet Service Provider

[15] eth1 is ignored

[16] http://fw.qmp.cat/

[17] http://qmp.cat/Supported_devices

[18] https://www.youtube.com/watch?v=xIflE_-V-B4\#t=50s

[19] http://wiki.ubnt.com/Firmware_Recovery

[20] http://www.qmp.cat/\#Use-the-firmware

[21] tftp info: http://wiki.openwrt.org/doc/howto/generic.flashing.tftp

[22] http://hugin.sourceforge.net/

[23] http://ca.wiki.guifi.net/wiki/Servidor_de_gr%C3%A0fiques_1

[24] http://wiki.openwrt.org/doc/howto/snmp.server

[25] Solució per apache 2.2 i 2.4: http://stackoverflow.com/questions/9127802