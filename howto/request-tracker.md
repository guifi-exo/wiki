Based on the debian experience. Debian uses 3 types of bug tracking systems: (1) [debbugs](https://en.wikipedia.org/wiki/Debbugs), (2) its [gitlab](https://gitlab.com) [instance](https://salsa.debian.org/public) and (3) [request tracker (rt)](https://bestpractical.com/request-tracker/) [instance](https://rt.debian.org/). debbugs and gitlab are more convenient for software issues and rt for service desk support. This guide explains how to install it for our organization.

This guide assumes you installed a mail server that you control (through a postfix aliases mechanism). Building mail server is complex but you worth it. At the moment we don't have a guide for that. [Decide what email system to use](https://rt-wiki.bestpractical.com/wiki/EmailInterface) and adapt this guide to your needs.

<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [Install](#install)
- [Configure](#configure)
  - [Problems and details about ResetPassword](#problems-and-details-about-resetpassword)
  - [TODO](#todo)
- [Design & Customizations](#design--customizations)
  - [Proposed generic structure](#proposed-generic-structure)
  - [Notify AdminCc when ticket arrives to your queue](#notify-admincc-when-ticket-arrives-to-your-queue)
  - [Autodispatcher to specific queue](#autodispatcher-to-specific-queue)
  - [Change status (lifecycles)](#change-status-lifecycles)
  - [Templates in catalan](#templates-in-catalan)
- [send test email](#send-test-email)
- [allow a trusted network to access the network](#allow-a-trusted-network-to-access-the-network)
- [Notes on time worked reports](#notes-on-time-worked-reports)
- [Permissions](#permissions)
  - [Easy: clear separation between users (unprivileged) and admins (privileged)](#easy-clear-separation-between-users-unprivileged-and-admins-privileged)
  - [Complex: allow some external collaborators to put hours](#complex-allow-some-external-collaborators-to-put-hours)
- [Known bug: request tracker keeps creating new issues](#known-bug-request-tracker-keeps-creating-new-issues)
- [Full text indexing and search](#full-text-indexing-and-search)
- [Maybe usefull / TODO](#maybe-usefull--todo)
- [Annex: migrating from mysql to postgresql](#annex-migrating-from-mysql-to-postgresql)
  - [extra (switch back to mysql config)](#extra-switch-back-to-mysql-config)
- [Annex: from stretch to buster](#annex-from-stretch-to-buster)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

# Install

The steps described [here](https://rt-wiki.bestpractical.com/wiki/DebianJessieInstallGuide) works too for debian stretch. This should deal with installation of all necessary apache2 packages follow the configuration for the database setup and rt root password:

    apt-get install request-tracker4 rt4-db-postgresql rt4-apache2 postgresql

Install extension to allow commands by email (reference https://metacpan.org/pod/RT::Extension::CommandByMail)

    cpan YAML
    cpan install    # I don't remember why this line
    cpan RT::Extension::CommandByMail

Install extension to allow reset password method (reference: https://metacpan.org/pod/RT::Extension::ResetPassword)

    cpan RT::Extension::ResetPassword

Install extension to have time worked reports (reference: https://metacpan.org/pod/RT::Extension::TimeWorkedReport). To install this extension I remember it was little bit tricky, first enter cpan:

    cpan

and inside cpan install it

    install RT::Extension::TimeWorkedReport

[Optional] Install extension to hide request tracker events in tickets. You can still see them in History tab (reference: https://metacpan.org/release/RT-Extension-BriefHistory; extra: https://rt-wiki.bestpractical.com/wiki/HideTransactions)

    cpan RT::Extension::BriefHistory

dialogue asks where is `RT.pm` in debian is `/usr/share/request-tracker4/lib/RT.pm`

# Configure

Put the rt config to apache

    cd /etc/apache2/conf-enabled/
    ln -s /etc/request-tracker4/apache2-modperl2.conf

Adapt configuration to our needs in `/etc/request-tracker4/RT_SiteConfig.d/50-debconf.pm`:

    # THE BASICS:

    Set($rtname, 'example.com');
    Set($Organization, 'orgexample');
    Set($Timezone, 'Europe/Madrid');

    Set($CorrespondAddress , 'rt@example.com');
    Set($CommentAddress , 'rt-comment@example.com');

    # THE WEBSERVER:

    Set($WebDomain, 'example.com');
    Set($WebBaseURL , "https://example.com");
    Set($WebPath , "/rt");

    #Set(@ReferrerWhitelist, qw(example.com:443, example.com:80));
    #lets try this -> src https://forum.bestpractical.com/t/cross-site-forgery/31763
    Set(@ReferrerWhitelist, qw());

Enable logs if you want in `/etc/request-tracker4/RT_SiteConfig.d/60-logging.pm` changing `Set($LogToFile , undef);` with `Set($LogToFile , 'debug');`

To create, reply and comment tickets through email with rt put in `/etc/aliases`:

    support: "|/usr/bin/rt-mailgate --queue general --action correspond --url http://localhost/rt"
    support-comment: "|/usr/bin/rt-mailgate --queue general --action comment --url http://localhost/rt"

and update aliases appropiately:

    newaliases && service postfix reload

To configure and enable the extensions that allow commands by email (change queue, set status, etc.) plus the extension for password reset you have to include the next three lines in `/etc/request-tracker4/RT_SiteConfig.pm`:

```
Plugin('RT::Extension::CommandByMail');
Set(@MailPlugins, qw(Auth::MailFrom Action::CommandByMail));
Set($CommandByMailHeader, "X-RT-Command");

 # to allow reset password method
Plugin('RT::Extension::ResetPassword');

 # to have time worked reports
Plugin('RT::Extension::TimeWorkedReport');

 # to hide request tracker events in tickets. You can still see them in History tab
Plugin('RT::Extension::BriefHistory');
 #After enabling or disabling briefhistory clear your mason cache
 #    rm -rf /opt/rt4/var/mason_data/obj/*

 # Use this to set the default units for time entry to hours instead of
 # minutes.  Note that this only effects entry, not display.
 # src https://fossies.org/linux/rt/etc/RT_Config.pm
Set($DefaultTimeUnitsToHours, 1);

 # #Infinite(?) body length in ticket responses. extra info here: http://requesttracker.8502.n7.nabble.com/quot-Message-body-not-shown-because-it-is-too-large-quot-td35515.html
 # Set($MaxInlineBody, 0);

 # avoid potential loops -> src https://rt-wiki.bestpractical.com/wiki/RTAddressRegexp
Set($RTAddressRegexp , '^help(-comment)?\@(help|admin).(example.org|ourother.domain.com)$');
```

Apply all configurations

    service apache2 reload

Configure RT-Shredder (ready to delete things) after that you can access it through: Admin / Tools / Shredder

    mkdir -p /var/lib/request-tracker4/data/RT-Shredder
    chown www-data -R /var/lib/request-tracker4/data/RT-Shredder

note: to delete comments using RT-Shredder get the transaction ID of the object and and declare it when deleting Object using Shredder -> src https://forum.bestpractical.com/t/unable-to-find-any-option-to-delete-an-message-from-the-rt-queue/32724/2

## Problems and details about ResetPassword

I reached this error in `/var/log/request-tracker4/rt.log`

> Couldn't load template 'PasswordReset' (/usr/local/share/request-tracker4/plugins/RT-Extension-ResetPassword/html/NoAuth/ResetPassword/Request.html:90)

if we inspect Templates in the web interface (example.com/rt/Admin/Global/Templates.html), it is not there the *PasswordReset: Send user an password reset token*. It could probably work just adding it, but I tried to do it in another way, using the `make initdb` (that it is just a command that looks like just adds the template). To do that I hit an error that assumed that the database admin was postgres in a mysql database (?)

restart service

    service apache2 restart

You have to run it this way to find the initialdata thing (change datafile accordingly if your cpan is in different place):

    /usr/bin/perl -Ilib -I/usr/local/share/request-tracker4/lib -I/usr/share/request-tracker4/lib /usr/sbin/rt-setup-database --action insert --datadir etc --datafile /root/.cpan/build/RT-Extension-ResetPassword-1.04*/etc/initialdata --dba root --prompt-for-dba-password  --package RT::Extension::ResetPassword --ext-version 1.04

in default's mariadb database for debian just press enter in password prompt

remember that if you want to introduce a user, you have to setup a random password so it can get one. If you try an autogenerated user to reset password it will say:

> You can't reset your password as you don't already have one.

## TODO

- Inmediate TODO [finish integration of the form](https://gitlab.com/guifi-exo/noc-forms) to avoid spam comming to the target email
- Rt can also handle [service level agreements](https://bestpractical.com/blog/2017/3/managing-service-level-agreements-slas-in-rt); but this part is not included in the guide (yet).
- https://rt-wiki.bestpractical.com/wiki/SpamFiltering
- better authentication https://docs.bestpractical.com/rt/4.4.1/authentication.html

# Design & Customizations

## Proposed generic structure

- Each queue handle different types of tickets.
- One queue (the dispatcher) receives all emails and its purpose is to route the tickets to the other queues.
- On each queue specific users and groups receive notifications because they are in the AdminCc

## Notify AdminCc when ticket arrives to your queue

source http://kb.mit.edu/confluence/display/istcontrib/Enabling+notification+to+your+team+when+a+ticket+is+moved+to+your+RT+queue

backup source https://web.archive.org/web/20190218160302/http://kb.mit.edu/confluence/display/istcontrib/Enabling+notification+to+your+team+when+a+ticket+is+moved+to+your+RT+queue

## Autodispatcher to specific queue

```
Condition: On Create
Action: User Defined
Template: Blank
Custom condition:
Custom action preparation code: `return 1;`
Custom action commit code:
```

Assuming that queueName in subject goes automatically to queueName queue:

```perl
 # src https://forum.bestpractical.com/t/auto-change-queue-based-on-requestor/15206/3
 # src https://rt-wiki.bestpractical.com/wiki/SetOwnerAndQueueBySubject

if (
 $self->TicketObj->Status  eq "new" &&
 $self->TicketObj->Queue   == 4 &&         # ID of suport queue
 # case insensitive https://stackoverflow.com/questions/9655164/regex-ignore-case-sensitivity
 $self->TicketObj->Subject =~ /queueName/i ) {
  $RT::Logger->error("change to alta queue");
  # thanks https://forum.bestpractical.com/t/simple-custom-action-gives-mason-compiler-error/4856/2
  my ($status, $msg) = $self->TicketObj->SetQueue("queueName");
  return 0;
}
```

## Change status (lifecycles)

https://docs.bestpractical.com/rt/4.4.4/customizing/lifecycles.html

## Templates in catalan

(using just the templates that are enabled by default)

Autoreply in HTML - HTML Autoresponse template:

```
Subject: {$Ticket->Subject}
Content-Type: text/html

<p>Hola,</p>

<p>Verifica si la petició que has enviat consta de la informació essencial per ser tramitada. Per afegir informació respon a aquest correu.</p>

<p>- Si es tracta d'una incidència: (1) Persona o organització vinculada a la connexió eXO i (2) descripció de la incidència: què és lo que no funciona i com proves de que no funciona. Opcionalment són útils els camps: (2) Nom del node BCNCarrerNúmero, (3) IP de l'emplaçament, (4) altres dades que puguis considerar d'interès</p>

<p>- Si es tracta d'un alta: (1) Nom i cognoms, (2) NIF, (3) Adreça de correu electrònic  (4) Formulari SEPA emplenat per autoritzar els rebuts mensuals https://exo.cat/wp-content/uploads/2017/02/Mandat-SEPA-QuotaSoci.pdf</p>

<p>Aquesta és una resposta automàtica generada en resposta a la teva petició que has titulat <b>{$Ticket->Subject()}</b>,
el teu resum del contingut que s'ha rebut es troba a baix.</p>

<p>No necessites respondre a aquest missatge de moment. El teu ticket se li ha assingat l'identificador <b>{$Ticket->SubjectTag}</b>.</p>

<p>En cas que vulguis respondre, respon a aquest correu i/o següents i incorpora <b>{$Ticket->SubjectTag}</b> com a títol en el email per a totes les futures comunicacions relacionades amb aquest ticket.</p>

<p>Descripció genèrica del servei (a menys que tingueu un document que digui un altre cosa)</p>

<ol>
<li>L'atenció tècnica remota que oferim es fa des de treball voluntari</li>
<li>La vostra quota de sòcia no inclou cap mena de servei comercial</li>
<li>Tampoc inclou cap mena de visita tècnica domiciliària a menys que esteu disposats a pagar el cost del desplaçament.</li>
<li>Els dispositius que teniu instal·lats a casa (tant el router wifi com l'antena del teulat) són de la vostra propietat</li>
</ol>

<p>SAX,<br/>
{$Ticket->QueueObj->CorrespondAddress()}</p>

<hr/>
{$Transaction->Content(Type => 'text/html')}
```

Resolved in HTML (catalan)

```
Subject: Resolved: {$Ticket->Subject}
Content-Type: text/html

<p>Segons ens consta el teu problema s'ha resolt.</p>

<p>Si respons a aquest correu el ticket es tornarà a obrir i l'haurem de tornar a resoldre. Respon si la incidència no s'ha resolt o si tens alguna pregunta relacionada amb la incidència.</p>
```

# send test email

put a file like `mailfile` with content:

```
To: support@example.com
From: user@example.com
Subject: test incident
Date: Sun, 21 Apr 2019 18:51:46 +0200

here the content of your email
```

the place you want to send the email, in this case is localhost (127.0.0.1):

    /usr/bin/rt-mailgate --queue General --action comment --url http://127.0.0.1/rt < mailfile

# allow a trusted network to access the network

this is useful if you want to relay mail with rt-mailgate to another host

supposing your trusted/internal network is 192.168.100.0/24, then in `/etc/request-tracker4/apache2-modperl2.conf` add:

```diff
<Location /rt/REST/1.0/NoAuth>
    <IfVersion >= 2.3>
        Require local
+       Require ip 192.168.100.0/24
    </IfVersion>
    <IfVersion < 2.3>
        Order Allow,Deny
        Allow from 127.0.0.1
+       Allow from 192.168.100.0/24
    </IfVersion>
</Location>
```

# Notes on time worked reports

The location of time worked reports is `Menu > Tools > TimeWorkedReport`. You can use this shortcut to have time already there:

https://example.com/rt/Tools/Reports/TimeWorkedReport.html?startdate=2018-01-01+00%3A00%3A00&enddate=2028-01-01+00%3A00%3A00&user=

if you process it with a user you get time worked reports for that specific user, if you process it with administrator then you can see it for all users

Users and queues are filters, without putting a filter is the total time

# Permissions

## Easy: clear separation between users (unprivileged) and admins (privileged)

Permissions for the unprivileged: configure queue's group rights with general permissions to Everyone: CommentOnTicket, CreateTicket, ShowTicket, SeeCustomField, SeeQueue, Watch, ReplyToTicket

Permissions for the privileged: create a group for staff and put all the members there. Configure queue's group rights of your newly created group to have all marked ticks for *General rights* and *Rights for Staff*

In summary, everyone can use the system to reply, but only the persons that are privileged and part of the group staff can operate the ticketing system

## Complex: allow some external collaborators to put hours

*Unprivileged users only get access to RT's self-service interface* (from book rt essentials). Unprivileged users have a simplified interface called self-service, they cannot change special information in a ticket (like worked hours), then, you need them to be privileged users.

But if you give them privileged status they can see all issues in the queue, and that's probably too much. We can adjust this: all privileged users related to a specific ticket can see it (not the entire queue). AdminCC and CC can operate on it.

Hence, customize the queue to be invisible to all, unmark: SeeCustomField, SeeQueue, ShowTicket. But mark all general permission for roles (when people is related to that issue): AdminCc, Cc, Owner, Requestor

Finally, add ModifyTicket for AdminCC and CC (from Rights for Staff tab) to allow putting work hours

# Known bug: request tracker keeps creating new issues

Some email workflow may use several times a specific topic very close to one of the already registered "Re: my issue". Request tracker will create a new issue when it processes it.

`[suport eXO #1] whatever` or responses like `Re: [suport eXO #1] whatever 2` works. So it is better to keep spontaneous discussions out of issue tracker and use issue tracker when required and just replying to the issue tracker email address putting the other in CC. Avoid reply all button.

thanks to http://requesttracker.8502.n7.nabble.com/User-Replies-to-a-forwarded-requests-and-receives-a-new-ticket-number-after-replying-td55430.html

# Full text indexing and search

setup full text indexing (default values are fine; press enter several times)

    rt-setup-fulltext-index-4 -dba rt_psql_user --dba-password 'rt_psql_user'

add in `/etc/request-tracker4/RT_SiteConfig.pm`

```
Set( %FullTextSearch,
    Enable     => 1,
    Indexed    => 1,
    Column     => 'ContentIndex',
    Table      => 'AttachmentsIndex',
);
```

run indexer each 30 mimnutes. Add to `/etc/cron.d/request-tracker4`

```
*/30 * * * * www-data [ -x /usr/sbin/rt-fulltext-indexer-4 ] && /usr/sbin/rt-fulltext-indexer-4
```

src https://docs.bestpractical.com/rt/4.2.12/full_text_indexing.html

# Maybe usefull / TODO

- https://metacpan.org/pod/RT::Action::AutoAddWatchers
- https://metacpan.org/pod/RT::Extension::RepliesToResolved

# Annex: migrating from mysql to postgresql

do an export of the mysql data in `~/rt_dump` directory, if you have to do this step again see the extra at the end (or backup the mysql config)

    rt-validator-4 --check && rt-serializer-4 --clone --directory ~/rt_dump

install db connector for postgresql and postgresql itself

    apt install rt4-db-postgresql postgresql

create user for postgresql

    su -s /bin/bash postgres -c "createuser rt_psql_user"

[enter postresql CLI](https://wiki.debian.org/PostgreSql#User_access):

    su -s /bin/bash postgres -c psql

[put password to user](https://stackoverflow.com/questions/12720967/how-to-change-postgresql-user-password)

    ALTER USER "rt_psql_user" WITH PASSWORD 'rt_psql_user';

[allow user to create databases](src https://stackoverflow.com/questions/43734650/createdb-database-creation-failed-error-permission-denied-to-create-database/43842881#43842881)

    alter user rt_psql_user createdb;
    \q

do this step if you are starting the procedure again (bad migration, etc.)

    su -s /bin/bash postgres -c "dropdb rt_psql_db"

put in `/etc/request-tracker4/RT_SiteConfig.d/51-dbconfig-common.pm`:

```perl
my %typemap = (
    mysql   => 'mysql',
    pgsql   => 'Pg',
    sqlite3 => 'SQLite',
);

Set($DatabaseType, $typemap{pgsql} || "UNKNOWN");

Set($DatabaseHost, 'localhost');
Set($DatabasePort, '5432');

Set($DatabaseAdmin, 'rt_psql_user');
Set($DatabaseUser , 'rt_psql_user');
Set($DatabasePassword , 'rt_psql_user');

 # SQLite needs a special case, since $DatabaseName must be a full pathname
my $dbc_dbname = 'rt_psql_db'; if ( "pgsql" eq "sqlite3" ) { Set ($DatabaseName, '' . '/' . $dbc_dbname); } else { Set ($DatabaseName, $dbc_dbname); }
```

create a database prepared to be populated with the imported settings

    rt-setup-database-4 --action create,schema,acl --prompt-for-dba-passwordw

service apache2 restart

## extra (switch back to mysql config)

if you have to do an export again, you have to switch back to the mysql configuration, here you can find an example. Put in `/etc/request-tracker4/RT_SiteConfig.d/51-dbconfig-common.pm`:

```perl
 # THE DATABASE:
 # generated by dbconfig-common

 # map from dbconfig-common database types to their names as known by RT
my %typemap = (
    mysql   => 'mysql',
    pgsql   => 'Pg',
    sqlite3 => 'SQLite',
);

Set($DatabaseType, $typemap{mysql} || "UNKNOWN");

Set($DatabaseHost, 'localhost');
Set($DatabasePort, '3306');

Set($DatabaseAdmin , 'root');
Set($DatabaseUser , 'rtuser');
Set($DatabasePassword , 'rtuser');

 # SQLite needs a special case, since $DatabaseName must be a full pathname
my $dbc_dbname = 'rtdb'; if ( "mysql" eq "sqlite3" ) { Set ($DatabaseName, '' . '/' . $dbc_dbname); } else { Set ($DatabaseName, $dbc_dbname); }
```

# Annex: from stretch to buster

upgrade postgresql manually

backup

    tar -c /var/lib/postgresql | gzip -c > postgresql-bak.tar.gz

upgrade database

    sudo -u postgres pg_upgradecluster 9.6 main $(pwd)/11.5

do indexing

    sudo -u postgres reindexdb --all
