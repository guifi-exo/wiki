<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [install](#install)
- [sharing](#sharing)
  - [iSCSI on ZVOL](#iscsi-on-zvol)
    - [client](#client)
      - [install](#install)
      - [configure](#configure)
      - [remove](#remove)
    - [server](#server)
      - [install](#install-1)
      - [configure](#configure-1)
      - [remove](#remove-1)
  - [ZFS via NFS](#zfs-via-nfs)
    - [server](#server-1)
    - [client](#client-1)
  - [misc: benchmark](#misc-benchmark)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

# install

TODO: update installation notes for buster, temp good guide: https://openzfs.github.io/openzfs-docs/Getting%20Started/Debian/Debian%20Buster%20Root%20on%20ZFS.html

if you are running debian you require stretch-backports:

    apt-get -t stretch-backports install zfs-dkms spl-dkms zfsutils-linux libnvpair1linux libuutil1linux libzfs2linux zfs-zed zfsutils-linux libzpool2linux spl

if you are running proxmox:

    apt-get install zfsutils zfs-initramfs

# sharing

Here we explain how we use a ZFS unit to get disks on VMs. Thanks Ramon Selga for sharing his knowledge, I hope this documentation captured the essence clearly.

general notes:

- iSCSI works fine but cannot be shared with other VMs
- NFS in sync mode is slow
- NFS in async mode is fast, but the storage volume cannot be used for sync operations such as database or exec
- samba: not tested but expected results similar to NFS

good general reference about sharing: https://pthree.org/2012/12/31/zfs-administration-part-xv-iscsi-nfs-and-samba/

## iSCSI on ZVOL

main reference: https://linuxlasse.net/linux/howtos/ISCSI_and_ZFS_ZVOL

### client

#### install

```
apt-get install open-iscsi
```

#### configure

Copy the InitiatorName and transfer it for the server

```
grep ^InitiatorName /etc/iscsi/initiatorname.iscsi
```

After the server is configured with the proper InitiatorName, continue:

```
server_ip=192.168.99.13
iscsiadm -m discovery -t sendtargets -p ${server_ip}
iqn='put here the resulting iqn shown by previous command'
iscsiadm --mode node --targetname ${iqn} --portal ${ip}:3260 --login
sed -i 's/node.startup = manual/node.startup = automatic/' /etc/iscsi/nodes/iqn*/*/default
```

when putting your device in `/etc/fstab` remember to include the argument `_netdev` to mount after setting up network during boot process, example:

```
+UUID=xXxXXxxx-Xxxx-xxXx-XXxxXXXxxXXxxXXxX	/iscsi_drive	myfavouritefs	_netdev,defaults	0	0
```

and mount the device

```
mount /iscsi_drive
```

#### reconnect

if the server gets down for some time you will get an `I/O error`, and you should logout and login (or reboot the server which is ready to autoload the service)

```
iscsiadm -m discovery -t sendtargets -p ${server_ip}
iqn='put here the resulting iqn shown by previous command'
sudo iscsiadm --mode node --targetname ${iqn} --portal ${server_ip}:3260 --logout
sudo iscsiadm --mode node --targetname ${iqn} --portal ${ip}:3260 --login
mount /iscsi_drive
```

#### remove

switch from automatic to manual

```
sed -i 's/node.startup = automatic/node.startup = manual/' /etc/iscsi/nodes/iqn*/*/default
reboot
```

(optional) remove the node (disk) from iSCSI client config

```
rm -rf /etc/nodes/iqn_you_want_to_delete
```

### server

#### install

```
apt-get install targetcli-fb open-iscsi
```

#### configure

```
vmid=vmid223
iscsi_server=192.168.99.13
size=1T
zfs_pool=rpool


zfs create -V ${size} ${zfs_pool}/${vmid}
targetcli /backstores/block create ${vmid} /dev/zvol/${zfs_pool}/${vmid}
targetcli /iscsi create

iqn='copy here the iqn line that generated the previous command'
targetcli /iscsi/${iqn}/tpg1/luns create /backstores/block/${vmid}
targetcli /iscsi/${iqn}/tpg1/portals/ delete ip_address=0.0.0.0 ip_port=3260
targetcli /iscsi/${iqn}/tpg1/portals/ create ip_address=${iscsi_server} ip_port=3260

InitiatorName='here goes the client iqn to give it exclusive access'
targetcli /iscsi/${iqn}/tpg1/acls create ${InitiatorName}
```

#### remove

after you verified the client stopped using the volume, do:

```
zfs_pool=rpool
vmid=vmid223
iqn='copy here the iqn from configuration step'

targetcli /iscsi/ delete ${iqn}
targetcli /backstores/block delete ${vmid}
zfs destroy ${zfs_pool}/${vmid}
```

## ZFS via NFS

### server

```
zfs_pool=rpool
vmid=vmid223
nfs_client=192.168.99.136
# choose sync or async
mode=async

zfs create -o mountpoint=/${vmid} ${zfs_pool}/${vmid}
zfs set quota=1T ${zfs_pool}/${vmid}
zfs set sharenfs="${mode},no_subtree_check,no_root_squash,rw=@${nfs_client}" ${zfs_pool}/${vmid}
```

verify the nfs share is available with `showmount -e`

### client

check client section on [nfs.md](./nfs.md) document

## misc: benchmark

this is the tool we used to check

```
apt install bonnie++
mountpoint=/vmid136_iscsi
vm_ram=2048
bonnie++ -d $mountpoint -u root -r $vm_ram -n 32
echo 'put the last long line of the previous command here to convert it from csv to html' | bon_csv2html
```
