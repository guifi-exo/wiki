# openLDAP

http://www.zytrax.com/books/ldap/ \
http://www.openldap.org/doc/admin24/ \
https://oav.net/mirrors/LDAP-ObjectClasses.html

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Install and configure](#install-and-configure)
  - [Install openLDAP server](#install-openldap-server)
  - [Basic config](#basic-config)
    - [Post install config](#post-install-config)
  - [Unique uid and mail attributes](#unique-uid-and-mail-attributes)
  - [Group membership and integrity](#group-membership-and-integrity)
  - [Add user and service group](#add-user-and-service-group)
  - [Password policy](#password-policy)
    - [Load the module](#load-the-module)
    - [Configure the overlay](#configure-the-overlay)
    - [Define password policies](#define-password-policies)
      - [Warning about certain password policies](#warning-about-certain-password-policies)
    - [olcPasswordHash: {SSHA512}](#olcpasswordhash-ssha512)
  - [LDAPS and Let's encrypt certs](#ldaps-and-lets-encrypt-certs)
    - [Force TLS](#force-tls)
- [Backup and Restore](#backup-and-restore)
  - [Dump databases](#dump-databases)
  - [Restore databases](#restore-databases)
- [Usage](#usage)
  - [Debug](#debug)
  - [Show config](#show-config)
  - [Change ldap admin config password](#change-ldap-admin-config-password)
  - [nobody user for ldap bind](#nobody-user-for-ldap-bind)
  - [Create a user using ldif](#create-a-user-using-ldif)
  - [Create a group using ldif](#create-a-group-using-ldif)
  - [Add a member to a group using ldif](#add-a-member-to-a-group-using-ldif)
  - [Delete a member from a group using ldif](#delete-a-member-from-a-group-using-ldif)
- [Apache Directory](#apache-directory)
  - [Useful connections to create with apache directory](#useful-connections-to-create-with-apache-directory)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Install and configure

## Install openLDAP server

    apt-get install slapd ldap-utils

`Enter Admin password: ****`
This will be the password for cn=admin,cn=config

    dpkg-reconfigure slapd

- `Omit OpenLDAP server configuration`: No
- `DNS domain name`: example.com
- `Organization name (base DN)`: dc=example,dc=com
- `Enter admin password`: `***************`
  - This will be the password for cn=admin,dc=example,dc=com
- `Database type`: mdb
  - default option

## Basic config

Edit `/etc/default/slapd`

We will use ldap on localhost, ldapi on localhost, and ldaps for connections from the outside.

```
SLAPD_SERVICES="ldap:/// ldaps:/// ldapi:///"
```

Find openldap config files here `/etc/ldap/slapd.d`

### Post install config

We import configuration directly into the DIT (Directory Information Tree) using the olc (Online configuration) syntax organized in files. So, let's create a directory to store those files

    mkdir /etc/ldap/ldif

## Unique uid and mail attributes

We want to avoid duplicate user ids. LDAP will accept uid=david,ou=group1 and uid=david,ou=group2 because they represent two different entries in the DIT. However, this may lead to uid confusion. We also wish the mail attribute to be unique for similar reasons.

Edit `/etc/ldap/ldif/unique.ldif`

We make uid and mail attributes unique across the `dc=example,dc=com` tree

```
# import the unique module library
dn: cn=module{1},cn=config
cn: module{1}
objectClass: olcModuleList
olcModuleLoad: unique
olcModulePath: /usr/lib/ldap

# define the module overlay
dn: olcOverlay={0}unique,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcUniqueConfig
olcOverlay: {0}unique
olcUniqueBase: dc=example,dc=com
olcUniqueAttribute: uid
olcUniqueAttribute: mail
```

And now we import the config

    ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/unique.ldif

## Group membership and integrity

We want to use groups of users to make permission assignment easier.

Edit `/etc/ldap/ldif/memberof.ldif`

```
# import the memberof module library
dn: cn=module{2},cn=config
cn: module{2}
objectClass: olcModuleList
olcModuleLoad: memberof
olcModulePath: /usr/lib/ldap

# configure the overlay
dn: olcOverlay={1}memberof,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcMemberOf
objectClass: olcOverlayConfig
objectClass: top
olcOverlay: {1}memberof
olcMemberOfDangling: ignore
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfNames
olcMemberOfMemberAD: member
olcMemberOfMemberOfAD: memberOf
```

And now we import the config

    ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/memberof.ldif

What happens with we add a user's DN to a group, and then at some later date delete that user? Well, the deleted user's DN remains as a member of the group. That's not good. We want some consistency.
First we import the library into `cn=module{2},cn=config`, the same DN we use for the memberof module.

Edit `/etc/ldap/ldif/refint-1.ldif`

```
# add the refint module to the existing cn=module{2},cn=config entry
dn: cn=module{2},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: refint
```

And now we import the modification

    ldapmodify  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/refint-1.ldif

Then we define the overlay configuration
Edit `/etc/ldap/ldif/refint-2.ldif`

```
# configure the refint overlay
dn: olcOverlay={2}refint,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcRefintConfig
objectClass: top
olcOverlay: {2}refint
olcRefintAttribute: memberof member manager owner
```

And import the configuration

    ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/refint-2.ldif

## Add user and service group

Edit `/etc/ldap/ldif/add-user-group.ldif`

```
dn: ou=users,dc=example,dc=com
objectClass: top
objectClass: organizationalUnit
ou: users
```

And import the configuration

    ldapadd -x -D 'cn=admin,dc=example,dc=com' -W -H ldapi:/// -f /etc/ldap/ldif/add-user-group.ldif

Edit `/etc/ldap/ldif/add-service-group.ldif`

```
dn: ou=services,dc=example,dc=com
objectClass: top
objectClass: organizationalUnit
ou: services
```

And import the configuration

    ldapadd -x -D 'cn=admin,dc=example,dc=com' -W -H ldapi:/// -f /etc/ldap/ldif/add-service-group.ldif

## Password policy

http://www.zytrax.com/books/ldap/ch6/ppolicy.html

We can define many password policies.

Password policy schema is not part of the default schema. We need to import it.

    ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif

### Load the module

Edit `/etc/ldap/ldif/pwdpolicy-1.ldif`

```
dn: cn=module{3},cn=config
objectClass: olcModuleList
cn: module{3}
olcModuleLoad: ppolicy.la
```

And import the config

    ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/pwdpolicy-1.ldif

### Configure the overlay

Edit `/etc/ldap/ldif/pwdpolicy-2.ldif`

```
dn: olcOverlay=ppolicy,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcPPolicyConfig
olcOverlay: ppolicy
# the dn can be anywhere in the tree, we have chosen this
olcPPolicyDefault: cn=defaultpwdpolicy,dc=example,dc=com
olcPPolicyHashCleartext: TRUE
olcPPolicyUseLockout: FALSE
olcPPolicyForwardUpdates: FALSE
```

And import the config

    ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/pwdpolicy-2.ldif

### Define password policies

Edit `/etc/ldap/ldif/pwdpolicy-3.ldif`

```
# add default policy to DIT
# attributes preceded with # indicate the defaults and
# can be omitted
# passwords must be reset every 1 year (pwdMaxAge 31536000)
# have a minimum length of 6 (pwdMinLength: 6), and users will
# get a expiry warning starting 1 week (pwdExpireWarning: 604800) before
# expiry, when the consecutive fail attempts exceed 5 (pwdMaxFailure: 5)
# the count will be locked for 5 minutes (pwdLockoutDuration: 300) before
# the user can login again, users do not need to supply the old
# password when changing (pwdSafeModify: FALSE)
# Users can change their own password (pwdAllowUserChange: TRUE)

dn: cn=defaultpwdpolicy,dc=example,dc=com
objectClass: pwdPolicy
objectClass: applicationProcess
objectClass: top
cn: defaultpwdpolicy
pwdMaxAge: 0
pwdExpireWarning: 604800
pwdAttribute: userPassword
pwdInHistory: 0
pwdCheckQuality: 1
pwdMaxFailure: 0
pwdLockout: FALSE
pwdLockoutDuration: 300
pwdGraceAuthNLimit: 0
pwdFailureCountInterval: 0
pwdMustChange: TRUE
pwdMinLength: 8
pwdAllowUserChange: TRUE
pwdSafeModify: FALSE
```

Just a note about `pwdLockout: TRUE` This can cause headaches when users have the wrong password in a client like the nextcloud desktop sync client. It will try to log in again and again, and eventually the account will be locked and the user will not be able to log in.

We import this ldif into the dc=example,dc=com tree, so we do it as the admin of that tree

    ldapadd -D "cn=admin,dc=example,dc=com" -W -f /etc/ldap/ldif/pwdpolicy-3.ldif

#### Warning about certain password policies

After a year, I've got a problem with `pwdMaxAge: 31536000`

I use a simpleSecurityObject `cn=nobody,dc=xxx,dc=xxx` that is used to bind. It's password has expired and now I have this error and intermitent binding fails on servers.

`ppolicy_bind: Entry cn=nobody,dc=example,dc=com has an expired password: 0 grace logins`

I created a User Specific Password Policy to be able to define pwdMaxAge to 0 just for the simpleSecurityObject. http://www.zytrax.com/books/ldap/ch6/ppolicy.html#examples
However I cannot add `pwdPolicySubentry: "cn=user,ou=pwpolicies,dc=example,dc=com"` to the" simpleSecutiryObject.

Until I find a solution I have changed `pwdMaxAge: 0` for everyone.

### olcPasswordHash: {SSHA512}

In the ppolicy config we set `olcPPolicyHashCleartext: TRUE`

Openldap uses SHA-1 and salted {SSHA} to hash passwords. We can use SHA-2 by importing the module.

Edit `/etc/ldap/ldif/pw-sha2_moduleload.ldif`

```
dn: cn=module,cn=config
objectClass: olcModuleList
cn: module
olcModuleLoad: pw-sha2.la
```

`ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/pw-sha2_moduleload.ldif`

And now make ssha512 the hash by default

edit `/etc/ldap/ldif/default_hash.ldif`

```
dn: olcDatabase={-1}frontend,cn=config
changetype: modify
replace: olcPasswordHash
olcPasswordHash: {SSHA512}
```

`ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/default_hash.ldif`

## LDAPS and Let's encrypt certs

*Use a guide to install letsencrypt certificates before continuing this guide*

Letsencrypt certs are stored at `/etc/letsencrypt/live/example.com` as symlinks but slapd needs a path to the files. Hence, you have to run jobs periodically.

Start creating a directory to place the dereferenced certificatest, for example here:

    mkdir -p /etc/slapd/tls

Use a cron.daily job to copy and dereference the links. Edit `/etc/cron.daily/letsencrypt_dereference_certs`

```
#!/bin/bash
/bin/cp -rpL /etc/letsencrypt/live/example.com/* /etc/slapd/tls/
```

NOTE: in debian (bullseye/testing) it can be helpful to delete the existing files before the copy, with "/bin/rm -rf /etc/slapd/tls/*", and to change the ownership of those files to the user "openldap". In this case the script above should be modified to:
```
#!/bin/bash
/bin/rm -rf /etc/slapd/tls/* && /bin/cp -rpL /etc/letsencrypt/live/example.com/* /etc/slapd/tls/ && /usr/bin/chown openldap /etc/slapd/tls/*
```
NOTE: again in debian (bullseye/testing) I had to add the openldap user to the ssl-cert group
```
usermod -a -G ssl-cert openldap
```

add executable permissions

    chmod +x /etc/cron.daily/letsencrypt_dereference_certs

Use a cron.weekly job to reload certificates on ldap server slapd. Edit `/etc/cron.weekly/slapd_letsencrypt_reload`

```
#!/bin/bash
/usr/sbin/service slapd force-reload
```
    chmod +x /etc/cron.weekly/slapd_letsencrypt_reload

Modify `cn=config` to include certificate configuration

Edit `/etc/ldap/ldif/tls.ldif`

```
dn: cn=config
changetype: modify
add: olcTLSCipherSuite
olcTLSCipherSuite: NORMAL
-
add: olcTLSCRLCheck
olcTLSCRLCheck: none
-
add: olcTLSVerifyClient
olcTLSVerifyClient: never
-
add: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/slapd/tls/fullchain.pem
-
add: olcTLSCertificateFile
olcTLSCertificateFile: /etc/slapd/tls/cert.pem
-
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/slapd/tls/privkey.pem
-
add: olcTLSProtocolMin
olcTLSProtocolMin: 3.3
```

Import modification

```
ldapmodify  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/tls.ldif
```

### Force TLS

Important:

- enabling force tls you cannot use ldapi nor ldap locally. It says `additional info: TLS confidentiality required`
- if you only enable port 636 you cannot connect externally through "no encryption" nor "starttls", then this option is not really needed

The slapd config would be something like this, but this forces TLS for both localhost and external connections.

Edit `/etc/ldap/ldif/force_tls.ldif`

```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
add: olcSecurity
olcSecurity: tls=1
```

Import modification

```
ldapmodify  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/force_tls.ldif
```

and restart the ldap server

    /etc/init.d/slapd restart

Instead, we can tell slapd to accept ldap:// only on localhost and and ldaps:// for external connections by binding to our server's IPs.

Our localhost IP is 127.0.0.1 and the network device eth0 has 10.200.161.1. Edit accordingly `/etc/default/slapd`:

    SLAPD_SERVICES="ldap://127.0.0.1/ ldaps://10.200.161.1/ ldapi:///"

Restart the service

    /etc/init.d/slapd restart

Don't forget to update your firewall

    iptables -A INPUT -p tcp -m tcp --dport 636 -j ACCEPT

# Tree permissions

Default openldap permission allow unauthenticated users to read the tree. Let's avoid that.

Edit `/tmp/access.ldif`

```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
olcAccess: {0}to attrs=userPassword by self write by anonymous auth by * none
olcAccess: {1}to attrs=shadowLastChange by self write by * read
olcAccess: {2}to * by * read
-
add: olcAccess
olcAccess: {0}to attrs=shadowLastChange by self write by * read
olcAccess: {1}to attrs=userPassword by self write by group.exact="cn=admins,dc=example,dc=com" write by anonymous auth by * none
olcAccess: {2}to * by self write by group.exact="cn=admins,dc=example,dc=com" write by users read
```

And import the config

    ldapmodify  -Y EXTERNAL -H ldapi:/// -f /tmp/access.ldif

Now lets create the 'admins' groupOfMembers. All users included in this group will have admin powers.

Edit `/tmp/add_group.ldif`

```
dn: cn=group_name,dc=example,dc=com
objectClass: top
objectClass: groupOfNames
cn: group_name
member: uid=existing_user,ou=users,dc=example,dc=com
```

Import configuration

    ldapadd -x -W -D "uid=my-user,dc=example,dc=com" -f /tmp/add_group.ldif


# Backup and Restore

## Dump databases

Dump config

    slapcat -b cn=config -l /tmp/cn=config.master.ldif

List your database(es) and dump

    slapcat -b cn=config | grep "^dn: olcDatabase=\|^olcSuffix"

Dump a database

    slapcat -b dc=example,dc=com -l /tmp/dc=example,dc=com.ldif

You might want to copy `/etc/default/slapd` and `/etc/ldap/ldap.conf` too.

## Restore databases

Stop slapd

    /etc/init.d/slapd stop

Move old files out of the way and create directories

```
mv /etc/ldap/slapd.d /tmp/
mkdir /etc/ldap/slapd.d
mv /var/lib/ldap /tmp
mkdir /var/lib/ldap
```

Import ldif files

```
slapadd -F /etc/ldap/slapd.d -b cn=config -l cn=config.master.ldif
-#################### 100.00% eta   none elapsed             03s spd 928.5 k/s
Closing DB...

slapadd -F /etc/ldap/slapd.d -b dc=example,dc=com -l dc=example,dc=com.ldif
-#################### 100.00% eta   none elapsed             03s spd 928.5 k/s
Closing DB...
```

Change owner of the files

```
chown -R openldap.openldap /etc/ldap/slapd.d
chown -R openldap.openldap /var/lib/ldap
```

And start slapd

    /etc/init.d/slapd start

# Usage

## Debug

stop ldap server service:

    service slapd stop

run it as a command:

    slapd -u openldap -d 256

[see here debug levels available](https://www.openldap.org/doc/admin24/runningslapd.html)

## Show config

    ldapsearch -LLL -Y EXTERNAL -H ldapi:/// -b olcDatabase={1}mdb,cn=config
slapcat -b cn=config

## Change ldap admin config password

You can change cn=admin,cn=config password like this

Create a file chg_admin_pass.ldif with content

```
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: foobar123
```

Then import the file

    ldapmodify -Y EXTERNAL -H ldapi:/// -f chg_admin_pass

## nobody user for ldap bind

Create nobody user to do the ldap bind when you integrate in the different services. Edit `/etc/ldap/ldif/create_nobody.ldif`

```
dn: cn=nobody,dc=example,dc=com
objectClass: simpleSecurityObject
objectClass: top
objectClass: organizationalRole
cn: nobody
userPassword: mypassword
```

And import the configuration

    ldapadd -x -D 'cn=admin,dc=example,dc=com' -W -H ldapi:/// -f /etc/ldap/ldif/create_nobody.ldif


## Create a user using ldif

Edit `/tmp/add_user.ldif`

```
dn: uid=peter,ou=users,dc=xrcb,dc=cat
objectClass: inetOrgPerson
cn: peter
sn: Surname
mail: peter@example.com
uid: peter
userPassword: 1234 
```

Import configuration

    ldapadd -x -W -D "uid=my-user,dc=example,dc=com" -f /tmp/add_user.ldif


## Create a group using ldif

Edit `/tmp/add_group.ldif`

```
dn: cn=group_name,dc=example,dc=com
objectClass: top
objectClass: groupOfNames
cn: group_name
member: uid=existing_user,ou=users,dc=example,dc=com
```

Import configuration

    ldapadd -x -W -D "uid=my-user,dc=example,dc=com" -f /tmp/add_group.ldif

## Add a member to a group using ldif

Edit `/tmp/add_member.ldif`

```
dn: cn=group_name,dc=example,dc=com
changetype: modify
add: member
member: uid=a_user,ou=users,dc=example,dc=com
```

Import configuration

    ldapmodify -x -W -D "uid=my-user,dc=example,dc=com" -f /tmp/add_member.ldif

## Delete a member from a group using ldif

Edit `/tmp/delete_member.ldif`

```
dn: cn=group_name,dc=example,dc=com
changetype: modify
delete: member
member: uid=a_user,ou=users,dc=example,dc=com
```

Import configuration

    ldapmodify -x -W -D "uid=my-user,dc=example,dc=com" -f /tmp/delete_member.ldif
 

# Apache Directory

To edit LDAP entries with a [GUI](https://en.wikipedia.org/wiki/Graphical_user_interface) use [Apache Directory](https://directory.apache.org/)

## Useful connections to create with apache directory

- tree view
- config view. Check that Base DN points change to cn=config. If you already configured the new connection go to `Properties -> Browser -> Options -> Base DN: cn=config`
