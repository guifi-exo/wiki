<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [synapse: retention policy one year](#synapse-retention-policy-one-year)
- [synapse: avoid joining complex rooms](#synapse-avoid-joining-complex-rooms)
- [synapse: adding new users to default room](#synapse-adding-new-users-to-default-room)
- [riot: overriding default jitsi server](#riot-overriding-default-jitsi-server)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

so I want to rebuild the matrix guide to be more generic and more pluggable for different needs

And I also need to keep documenting it

here are explained extra options

## synapse: retention policy one year

data stored by matrix grows and grows, you can limit the retention for a specific time interval, here it is for one year of persistence

```
# Retention policy limits. If set, a user won't be able to send a
# 'm.room.retention' event which features a 'min_lifetime' or a 'max_lifetime'
# that's not within this range. This is especially useful in closed federations,
# in which server admins can make sure every federating server applies the same
# rules.
#

retention:
  enabled: true
  default_policy:
    min_lifetime: 1d
    max_lifetime: 1y

  # If no configuration is provided, a single job will be set up to delete expired
  #   # events in every room daily.
  purge_jobs:
    - longest_max_lifetime: 1y
      interval: 1w
```

## synapse: avoid joining complex rooms

this is important to reduce the resources used by matrix

```
# Resource-constrained homeserver Settings
#
# If limit_remote_rooms.enabled is True, the room complexity will be
# checked before a user joins a new remote room. If it is above
# limit_remote_rooms.complexity, it will disallow joining or
# instantly leave.
#
# limit_remote_rooms.complexity_error can be set to customise the text
# displayed to the user when a room above the complexity threshold has
# its join cancelled.
#
# Uncomment the below lines to enable:
limit_remote_rooms:
  enabled: true
  complexity: 1.0
  complexity_error: "limit_remote_rooms is enabled in synapse with complexity=1 - contact administrator or complain publicly"
```

## synapse: adding new users to default room

in synapse yaml config

```yaml
# Users who register on this homeserver will automatically be joined
# to these rooms
auto_join_rooms:
    - "#example:example.com"
```

src https://github.com/matrix-org/synapse/issues/2398

## riot: overriding default jitsi server

in riot json config

(right now this is not very polished nor recommended)

```json
    "jitsi": {
        "preferredDomain": "meet.guifi.net"
    }
```

src https://matrix.org/blog/2020/04/06/running-your-own-secure-communication-service-with-matrix-and-jitsi

## riot: important tuning config for web server

the riot-web's bundle.js is 4.5 MB as of today, that means, a slow client

with gzip compression you can make it faster, as it is only 1.5MB~ transferred (three times less)

[but remember that enabling gzip could be bad for your security](https://michael.lustfield.net/nginx/getting-a-perfect-ssl-labs-score)

to benefit from it in nginx, make sure the following lines are uncommented:

```
gzip on;
gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;
```

## matrix-riot: well-known files (optional)

sometimes you see file not found both from serverside and clientside

for the client, in file `/var/www/html/.well-known/matrix/client`, add:

```
{
    "m.homeserver": {
        "base_url": "https://matrix.example.com"
    },
    "m.identity_server": {
        "base_url": "https://matrix.example.com"
    }
}
```

for the server, in file `/var/www/html/.well-known/matrix/server`, add:

```
{"m.server": "matrix.example.com:443"}
```
