#!/bin/bash

# Authors: @MTRNord:matrix.ffslfl.net @grigruss:matrix.org
# forked from https://github.com/grigruss/Riot-web-server-update/commit/59e8fcc7d25bc7ac25ba0a943ac50ba6a488f7bb

# Specify the path to the web server directory
ELEMENT_WEB_DIR="/var/www/html/element-web"
# go to directory 
cd "$ELEMENT_WEB_DIR"

# Get the content to determine the latest version.
content=$(curl -s https://api.github.com/repos/vector-im/element-web/releases/latest)
package_id=$(jq -r '.id' <<<"$content")

# If it is started for the first time, it creates a file in which the latest version number will be saved.
[ -f ./element_version-id ] || touch element_version-id

# Ensure existens of the ELEMENT_WEB_DIR directory
if [ ! -d $ELEMENT_WEB_DIR ]
then
    mkdir $ELEMENT_WEB_DIR
fi

# If the versions are different, we begin the update
if [ "$package_id" != "$(cat ./element_version-id)" ]
then
    download_url=$(jq -r '.assets[] | select((.content_type == "application/x-gzip") or (.content_type == "application/octet-stream")) | .browser_download_url | select(contains("asc") | not)' <<<"$content")
    if [ "$download_url" != "" ]
    then
        # If there is no Element-web directory, it will be created.
        if [ ! -d ./Element-web ]
        then
            mkdir Element-web
        fi

        echo "New Version found starting download"
        curl -Ls "$download_url" | tar xz --strip-components=1 -C ./Element-web/
        echo "$package_id" > ./element_version-id
        echo "The new version is downloaded. Copying to the web server directory begins."

        # Uncomment for save your logos
        # rm -rf ./Element-web/img/logos

        # Copy the new version of Element to the web server directory
        cp -r ./Element-web/* $ELEMENT_WEB_DIR
        # Delete the new version from the Element-web buffer directory
        rm -rf ./Element-web
        echo "Copying to the web server directory finished. Exiting..."
        exit 0
    else
        echo "New version doesn't contain needed archive. Aborting..."
        exit 1
    fi
fi
