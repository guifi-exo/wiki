#!/bin/bash

# force all variables to be defined
set -u

# get variables (modify variables to change behavior)
source send_message.env

# pass arguments as message to be sent if are not null -> src https://stackoverflow.com/questions/2427995/bash-no-arguments-warning-and-case-decisions/2428050#2428050
if [ ! -z "$*" ]; then
  message="$@"
fi

# scape characters to avoid malformed post request
message=$(echo $message|sed 's/\\/\\\\/g');
message=$(echo $message|sed 's/"/\\"/g');

# message without html tags
msg=$(echo $message|sed 's/<[^<]*>//g');

# thanks https://stackoverflow.com/questions/17029902/using-curl-post-with-variables-defined-in-bash-script-functions/17032673#17032673
generate_post_data()
{
  cat <<EOF
{
  "msgtype": "m.text",
  "body": "$msg",
  "format": "org.matrix.custom.html",
  "formatted_body": "$message"
}
EOF
}

url="https://${matrix_homeserver}/_matrix/client/r0/rooms/${matrix_room}/send/m.room.message?access_token=${access_token}"

curl -POST -k -d "$(generate_post_data)" "$url"
