#!/bin/sh
# Copyright (c) 2020 guifipedro@cas.cat
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

# prepare VM following the policy for exo.cat

# prepare cloudinit images -> src https://pve.proxmox.com/wiki/Cloud-Init_Support#_preparing_cloud_init_templates

# Parse input parameters
usage() {
  echo "Usage: $0 --vm <VMID> --image|-i <image.qcow2> --store|-s <proxmox-storage> [options]
Options are:
 --name|-n <VM's name> (default: cloudinit-template)
 --bridge|-b <Bridge name> (default: vmbr0)
 --vlan <VLAN's bridge name, if not specified it is untagged> (default: untagged)
 --ram|-r <VM's RAM> (default: 512 MB)"
  exit 1
}

NAME="cloudinit-template"
BRIDGE="vmbr0"
RAM=512

while [ $# -gt 0 ]; do
  case "${1}" in
  "--vm")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the VMID"
      usage
    fi
    VMID=${2}
    shift
    shift
    ;;
  "--image"|"-i")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the Image"
      usage
    fi
    IMAGE=${2}
    shift
    shift
    ;;
  "--store"|"-s")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the Storage"
      usage
    fi
    STORE=${2}
    shift
    shift
    ;;
  "--name"|"-n")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the NAME"
      usage
    fi
    NAME=${2}
    shift
    shift
    ;;
  "--bridge"|"-b")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the Bridge network interface"
      usage
    fi
    BRIDGE="${2}"
    shift
    shift
    ;;
  "--vlan")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the VLAN"
      usage
    fi
    VLAN=",tag=${2}"
    shift
    shift
    ;;
  "--ram"|"-r")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the RAM"
      usage
    fi
    RAM=${2}
    shift
    shift
    ;;
  *)
    echo "Unrecognized option ${1}"
    usage
    ;;
  esac
done

# Check mandatory parameters
if [ -z "${VMID}" ] ; then
  echo 'VMID not recognized: please specify the --vm parameter.'
  usage
fi
if [ -z "${IMAGE}" ] ; then
  echo 'Image not recognized: please specify the -i parameter.'
  echo '  Get an image in https://cdimage.debian.org/cdimage/openstack or'
  echo '  construct it yourself with `build-openstack-debian-image` script'
  usage
fi
if [ -z "${STORE}" ] ; then
  echo 'Store not recognized: please specify the -s parameter.'
  usage
fi
if [ -z "${VLAN}" ] ; then
  VLAN=''
fi

# create a new VM with that particular needs
qm create "$VMID" --name "$NAME" --net0 virtio,bridge="${BRIDGE}${VLAN}"

# apply eXO standards to VM
#   also configure a serial console and use it as a display. Many Cloud-Init images rely on this, as it is an requirement for OpenStack images.
#qm set "$VMID" --cpu host --memory "$RAM" --ostype l26 --balloon 0 --serial0 socket --vga serial0
# vga serial problematic
# TODO but not defined, problematic?
qm set "$VMID" --cpu host --memory "$RAM" --ostype l26 --balloon 0 --serial0 socket

# import the downloaded disk to VM storage
qm importdisk "$VMID" "$IMAGE" "$STORE" --format qcow2

# finally attach the new disk to the VM as scsi drive
disk_path="$(qm config $VMID | grep unused0 | awk '{print $2}')"
qm set "$VMID" --scsihw virtio-scsi-pci --scsi0 "$disk_path,cache=writeback"

# add cloud-init drive
# The next step is to configure a CDROM drive which will be used to pass the Cloud-Init data to the VM.
qm set "$VMID" --ide2 "${STORE}:cloudinit"

# To be able to boot directly from the Cloud-Init image, set the bootdisk parameter to scsi0, and restrict BIOS to boot from disk only. This will speed up booting, because VM BIOS skips the testing for a bootable CDROM.
qm set "$VMID" --boot c --bootdisk scsi0

# depending on the storage system, you can get a message such as
#   `/usr/bin/chattr: Function not implemented while reading flags on /path/to/disk.qcow2`
qm template "$VMID" || true
