#!/bin/sh -e

# temp way to provision a VM that uses netdb
usage() {
  echo "Usage: $0 --target <target VM> --vlan <VLAN's bridge in target VM>"
  exit 1
}

while [ $# -gt 0 ]; do
  case "${1}" in
  "--target")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the target VMID"
      usage
    fi
    TARGET="${2}"
    shift
    shift
    ;;
  "--vlan")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the VLAN"
      usage
    fi
    VLAN="${2}"
    shift
    shift
    ;;
  *)
    echo "Unrecognized option ${1}"
    usage
    ;;
  esac
done

if [ -z "${TARGET}" ] ; then
  echo 'Target not recognized: please specify the --target parameter.'
  usage
fi

if [ -z "${VLAN}" ] ; then
  echo 'VLAN not recognized: please specify the --vlan parameter.'
  usage
fi

# debian
TEMPLATE=9000
# ubuntu18
#TEMPLATE=9009
SSH_HOST=exo-trax8
SSH_KEYS=ssh-keys-pedro
# download db from internet
NETDB_URL="https://gitlab.com/guifi-exo/public/-/raw/master/infrastructure/acer30/db/network.yml?inline=false"
DB="$(curl -s ${NETDB_URL})"
# pass arg -> src https://stackoverflow.com/questions/40027395/passing-bash-variable-to-jq
# arg to number -> https://stackoverflow.com/questions/41772776/numeric-argument-passed-with-jq-arg-not-matching-data-with
HOST_PARAMS="$(echo "$DB" | yq --arg vmid "$TARGET" -r '.["vms_exo","vms_3parties"] | .[] | select(.vmid == ($vmid | tonumber) )')"
if [ -z "${HOST_PARAMS}" ] ; then
  echo "No VMID ${TARGET} found on netdb, check your netdb is valid and published"
  exit 1
fi
prefix='__netdb_'
netdb_vars_raw=$(echo "$HOST_PARAMS" | jq -r "to_entries|map(\"__netdb_\(.key)=\(.value|tostring)\")|.[]")
# sanitize netdb_vars
netdb_vars=$(echo "$netdb_vars_raw" | tr -d '"[]{}' )
# json to env -> src https://stackoverflow.com/questions/48512914/exporting-json-to-environment-variables
for var in ${netdb_vars}; do
    # only accept vars with appropriate prefix
    if echo $var | grep -q "$prefix"; then
      export $var
    fi
done
IP="$(echo "$HOST_PARAMS" | yq --arg vlan "vlan${VLAN}" -r '.[$vlan].ip')"
GW="$(echo "$DB" | yq --arg vlan "vlan${VLAN}" -r '.["vlan_plan"] | .[$vlan].gateway')"

if [ -z "${IP}" ] || [ "${IP}" = "null" ]; then
  echo "No IP found on VM ${TARGET}, check your netdb config and/or VLAN ${VLAN}"
  exit 1
fi
if [ -z "${GW}" ] || [ "${GW}" = "null" ]; then
  echo "No GW found on netdb, check your netdb and paramteres are valid"
  exit 1
fi

ssh ${SSH_HOST} ./create_vm_cloudinit.sh --template ${TEMPLATE} --target ${TARGET} --target-storage vm_nvme --ip ${IP} --gateway ${GW} --ssh-keys ${SSH_KEYS} --name ${__netdb_hostname} --vlan ${VLAN}
