#!/bin/sh
# Copyright (c) 2020 guifipedro@cas.cat
# SPDX-License-Identifier: GPL-3.0-or-later

# src https://pve.proxmox.com/wiki/Cloud-Init_Support#_deploying_cloud_init_templates

set -e

# Parse input parameters
usage() {
  echo "Usage: $0 --template <template VMID> --target <target VM> --ip <IP in target VM> --ssh-keys <path to file with ssh keys without comments nor empty lines> [options]
Options are:
 --target-storage <target storage for VM's disk>
 --gateway|-g <VM's gateway iface>
 --dns-server|-d <VM's DNS server>
 --name|-n <VM's target name> (default: VMtest-from-cloudinit-template)
 --bridge|-b <Bridge in target VM> (default: vmbr0)
 --vlan <VLAN's bridge in target VM, if not specified it is untagged> (default: untagged)
 --start|-s (start VM when steps finished)
 --env|-e load parameters from file"
  exit 1
}

# default params
NAME="VMtest-from-cloudinit-template"
BRIDGE="vmbr0"
TARGET_STORAGE=""

# handle arguments -> src https://docstore.mik.ua/orelly/unix3/upt/ch35_22.htm
while [ $# -gt 0 ]; do
  case "${1}" in
  "--template")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the target VMID"
      usage
    fi
    TEMPLATE="${2}"
    shift
    shift
    ;;
  "--target")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the target VMID"
      usage
    fi
    TARGET="${2}"
    shift
    shift
    ;;
  "--target-storage")
    if [ -z "${2}" ] ; then
      echo "No target storage defined"
      usage
    fi
    TARGET_STORAGE="--storage ${2}"
    shift
    shift
    ;;
  "--ip")
    # check valid IP (only linux) -> src https://unix.stackexchange.com/a/581081
    # check includes '/' character -> src https://stackoverflow.com/questions/229551/how-to-check-if-a-string-contains-a-substring-in-bash/20460402#20460402
    if ip route get "${2}" 2>&1 >/dev/null && [ ! -n "${2##*/*}" ] ; then
      true
    else
      echo "No valid IP parameter in CIDR format for target VM"
      usage
    fi
    IP="${2}"
    shift
    shift
    ;;
  "--gateway"|"-g")
    if ip route get "${2}" 2>&1 >/dev/null && [ -n "${2##*/*}" ] ; then
      true
    else
      echo "No valid Gateway IP parameter (no CIDR format) for target VM"
      usage
    fi
    GATEWAY_OPT=",gw=${2}"
    shift
    shift
    ;;
  "--dns-server"|"d")
    if ip route get "${2}" >/dev/null 2>&1 && [ -n "${2##*/*}" ] ; then
      true
    else
      echo "No valid DNS IP parameter (no CIDR format) for target VM"
      usage
    fi
    DNS="${2}"
    shift
    shift
    ;;
  "--ssh-keys")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the ssh-keys for target VM"
      usage
    fi
    if [ ! -f "${2}" ] ; then
      echo "File referred in --ssh-keys ${2} does not exist or is not a file"
      usage
    fi
    if grep -E '#|^$' ${2} ; then
      echo "File referred in --ssh-keys ${2} contains"
      echo "  comments (#) or white lines, remove them"
      usage
    fi
    SSH_KEYS="${2}"
    shift
    shift
    ;;
  "--name"|"-n")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the NAME"
      usage
    fi
    NAME=${2}
    shift
    shift
    ;;
  "--bridge"|"-b")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the Bridge network interface"
      usage
    fi
    BRIDGE="${2}"
    shift
    shift
    ;;
  "--vlan")
    if ! [ ${2} -gt 0 ] 2> /dev/null; then
      echo "No parameter or positive number defining the VLAN"
      usage
    fi
    VLAN_OPT=",tag=${2}"
    shift
    shift
    ;;
  "--start")
    START=yes
    shift
    ;;
  "--env")
    if [ -z "${2}" ] ; then
      echo "No parameter defining the env"
      usage
    fi
    if [ ! -f "${2}" ] ; then
      echo "File referred in --env ${2} does not exist or is not a file"
      usage
    fi
    . "$(readlink -f "${2}")"
    shift
    shift
    ;;
  *)
    echo "Unrecognized option ${1}"
    usage
    ;;
  esac
done

# Check mandatory parameters
if [ -z "${TEMPLATE}" ] ; then
  echo 'Template not recognized: please specify the --template parameter.'
  usage
fi
if [ -z "${TARGET}" ] ; then
  echo 'Target not recognized: please specify the --target parameter.'
  usage
fi
if [ -z "${IP}" ] ; then
  echo 'IP not recognized: please specify the --ip parameter.'
  usage
fi
if [ -z "${SSH_KEYS}" ] ; then
  echo 'ssh keys file not recognized: please specify the --ssh-keys parameter.'
  usage
fi
if [ -z "${VLAN_OPT}" ] ; then
  VLAN_OPT=''
fi

qm clone "$TEMPLATE" "$TARGET"  "$TARGET_STORAGE" --name "$NAME" --full yes

if [ -z "${DNS}" ] ; then
  qm set "$TARGET" --ipconfig0 ip="$IP""$GATEWAY_OPT"
else
  qm set "$TARGET" --ipconfig0 ip="$IP""$GATEWAY_OPT" --nameserver "$DNS"
fi

# note that Proxmox VE currently will not accept comment lines. -> src https://pve.proxmox.com/wiki/Cloud-Init_FAQ#SSH_public_key
qm set "$TARGET" --sshkey "$SSH_KEYS"

qm set "$TARGET" --net0 virtio,bridge="${BRIDGE}${VLAN_OPT}"

# enable qemu-guest-agent -> src https://pve.proxmox.com/wiki/Qemu-guest-agent
qm set "$TARGET" --agent 1

if [ -n "${START}" ] ; then
  qm start "$TARGET"
fi
