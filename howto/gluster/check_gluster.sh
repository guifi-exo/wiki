#!/bin/sh -e

echo =========
for g in $(gluster v list); do
	echo volume $g
	gluster volume heal $g info | grep entries | sort -u
	echo =========
done
