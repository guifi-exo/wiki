# gluster

## check gluster's health

when shutting down and powering up the gluster node needs data reconstruction (selfheal). Check that all numbers are 0. Use [check_gluster.sh](./check_gluster.sh)

example output

```
root@host1:~# ./check_gluster.sh
=========
volume vmhddstore
Number of entries: 0
=========
volume vmssdstore
Number of entries: 0
=========
```

## troubleshoot gluster

so gluster is taking too much time for recovery and you want to know what are the file shards and VM disks that are affecting it

```
gluster v heal mystore info | sort -u
```

the more entries, the more gluster have to work with that disk image

use script [check_disk_gluster.sh](./check_disk_gluster.sh)

```
root@host02:~# ./check_disk_gluster.sh
===============================================================
  # processing gfid 396c57c5-6114-4365-867c-6a4912562589
    ## du size
664M	/vmhddstore/images/554/vm-554-disk-0.raw
    ## du apparent size
90G	/vmhddstore/images/554/vm-554-disk-0.raw
    ## check image
qemu-img: This image format does not support checks
===============================================================
  # processing gfid 60e61d22-8564-4e0a-bdb4-d13c220ea251
    ## du size
245M	/vmhddstore/images/5002/vm-5002-disk-1.qcow2
    ## du apparent size
11G	/vmhddstore/images/5002/vm-5002-disk-1.qcow2
    ## check image
No errors were found on the image.
168203/524288 = 32.08% allocated, 0.02% fragmented, 0.00% compressed clusters
Image end offset: 11026104320
===============================================================
  # processing gfid af524a3f-23b5-4c1a-a5c4-845d34c270a9
NOT FOUND: this gfid is not present in that host
===============================================================
  # processing gfid bd51440a-b0e8-4d26-b84e-68cb7ce05bcb
    ## du size
464M	/vmhddstore/images/5217/vm-5217-disk-0.raw
    ## du apparent size
34G	/vmhddstore/images/5217/vm-5217-disk-0.raw
    ## check image
qemu-img: This image format does not support checks
===============================================================
```
