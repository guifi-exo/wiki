# LDAPS and Let's encrypt certs

Our Certs are stored in `/etc/letsencrypt/ldap.example.com/`

Modify `cn=config` to include certificate configuration

`Edit /etc/ldap/ldif/cert.ldif`

```
dn: cn=config
changetype: modify
add: olcTLSCipherSuite
olcTLSCipherSuite: NORMAL
-
add: olcTLSCRLCheck
olcTLSCRLCheck: none
-
add: olcTLSVerifyClient
olcTLSVerifyClient: never
-
add: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/letsencrypt/ldap.example.com/fullchain.pem
-
add: olcTLSCertificateFile
olcTLSCertificateFile: /etc/letsencrypt/ldap.example.com/cert.pem
-
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/letsencrypt/ldap.example.com/privkey.pem
-
add: olcTLSProtocolMin
olcTLSProtocolMin: 3.3
```

Import modification

    ldapmodify  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/cert.ldif

## Force TLS

Need to double check all of this again!

The slapd config would be something like this, but this forces TLS for both localhost and external connections.

```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
add: olcSecurity
olcSecurity: tls=1
```

Instead, we can tell slapd to accept ldap:// only on localhost and and ldaps:// for external connections by binding to our server's IPs.

Edit `/etc/default/slapd`

our localhost IP is 127.0.0.1 and the network device eth0 has 10.200.161.1

    SLAPD_SERVICES="ldap://127.0.0.1/ ldaps://10.200.161.1/ ldapi:///"

    /etc/init.d/slapd restart

Dont forget to update your firewall

    iptables -A INPUT -p tcp -m tcp --dport 636 -j ACCEPT
