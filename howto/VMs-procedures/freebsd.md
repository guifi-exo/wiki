- ~~Do not enable qemu-agent, with qemu-agent enabled, it tries to send the shutdown signal through it, without it, it does it through acpi. Right now, there is no clear support for qemu-agent~~ [qemu-guest-agent](https://www.freshports.org/emulators/qemu-guest-agent) works fine on FreeBSD 13
- Hot plugged virtio interfaces are not seen, `reboot`
- Network configuration cannot be reload, you should restart:
  - restart everything `service netif restart`
  - restart specific iface `service netif restart vtnet0`
- Do not change default settings of `/root/.cshrc` or its shell, if you want bash, install bash and use it when needed
  - to login directly with bash, use `ssh -t yourHost bash`
- From FreeBSD side, to enable serial connection add in file `/boot/load.conf` line `console="comconsole"`
- Boot by default takes 10 seconds in initial boot menu, to modify it go to file `/boot/load.conf` and add line `autoboot_delay="1"`

mini rosetta code linux-freebsd ([more](https://github.com/AnotherKamila/linux2bsd))

| description | linux | freebsd |
| ----------- | ----- | ------- |
| show main routing table for IPv4 | ip r | netstat -4rW |
| show main routing table for IPv6 | ip -6 r | netstat -6rW |
| show application of routing table for targetHost | ip route get targetHost | route show targetHost |

# resource policy

- rootfs 10 GB
- 1 core CPU
- 1 GB RAM
  - To use ZFS, at least 1 GB of memory is recommended (for all architectures) but more is helpful as ZFS needs lots of memory. Depending on your workload, it may be possible to use ZFS on systems with less memory, but it requires careful tuning to avoid panics from memory exhaustion in the kernel https://wiki.freebsd.org/ZFSTuningGuide


# things we put on the freebsd 13 template

```
# src https://www.freshports.org/net/cloud-init/
pkg install py38-cloud-init-21.4

pkg install tmux
pkg install vim

# src https://www.freshports.org/emulators/qemu-guest-agent
pkg install qemu-guest-agent
```

- cloud-init activated complains (hangs during boot, or hangs for some time) if cloudinit drive is not available. With direct access to the host and `Ctrl+c` you can skip it
- qemu-guest-agent and py38-cloud-init-21.4 throw messages after the installation with the steps to activate it, just follow them

reduce autoboot time from 10 seconds to 2 -> src https://www.nesono.com/content/reduce-freebsd-boot-time

Add on file `/boot/loader.conf` a line with `autoboot_delay="2"`

# other resources and references

- [Guifilab] [CAT] En-ruta amb FreeBSD https://www.youtube.com/watch?v=ilcjyybGIK8
  - guides on installing Freebsd 13
