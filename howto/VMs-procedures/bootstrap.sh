#!/bin/sh -e

# Get the latest version from
# https://exo.cat/bootstrap.sh
# (307 redirect)

# Ensure apt's cache is properly setup
sed -i'' -E 's/^deb[[:space:]]+cdrom:.*$//g' /etc/apt/sources.list
apt-get update

# Disable apt's install recommends, not having this is making server
# installations end up with things like X11
cat > /etc/apt/apt.conf.d/01norecommend << EOF
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF

# Basic system utilities
apt-get install -y vim tmux screen tcpdump nmap cloud-init qemu-guest-agent sudo parted xfsprogs openssh-server

# Harden users a bit
#   PasswordAuthentication no
sed -i'' -E 's/^[#]?[[:space:]]*PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config

# Allow root access for provisioning
#   PermitRootLogin prohibit-password
sed -i'' -E 's/^[#]?[[:space:]]*PermitRootLogin.*/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config

# It could be that ssh has not generated the keys so
#   a restart would not work as expected
service sshd stop || service ssh stop || true
service sshd start || service ssh start

# Lower GRUB timeout (this is a VM)
sed -i'' -E 's/^[#]?[[:space:]]*GRUB_TIMEOUT=[0-9]+$/GRUB_TIMEOUT=1/g' /etc/default/grub
# reload grub config
update-grub

# Ensure /etc/network/interfaces does not interfere with cloud-init
cat > /etc/network/interfaces <<EOF
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug eth0
# We do not set up eth0 IP here, cloud-init should take care of that
# See /etc/network/interfaces.d/50-cloud-init
EOF

# Disable cloud-init after first boot
cat > /etc/cloud/cloud.cfg.d/99_exo.cfg <<EOF
# cloudinit disables by default public key ssh access with the root user
#   we explicitly allow that
disable_root: false

# eXO: Disable cloud-init after first configuration run
#   src https://cloudinit.readthedocs.io/en/latest/topics/examples.html#run-commands-on-first-boot
runcmd:
  - [ touch, /etc/cloud/cloud-init.disabled ]
EOF
# Note that if we are modifying a template, we should perform
#     rm /etc/cloud/cloud-init.disabled
#   before shutting it down
if [ -f /etc/cloud/cloud-init.disabled ]; then
    rm -f /etc/cloud/cloud-init.disabled
fi
