# nodo frontera (bmx6 + bird)

solemos usar arquitectura de dispositivo x86_64 en forma de máquina virtual o apu2, se puede hacer funcionar en otras arquitecturas.

el dispositivo no hace falta que tenga wifi/radio

en esta guía vamos a considerar, por simplicidad, tantas interfaces de red físicas como enlaces a establecer. con un switch gestionable de soporte para transportar VLANs allá donde sea:

- interfaz bmx6:
  - conecta con la mesh bmx6 por cable o wifi
  - para más información sobre el diseño de bmx6 consultar [temba](https://gitlab.com/guifi-exo/temba)
- interfaz wan:
  - conexión a internet para establecer L2TP con servidores remotos
  - se establece un peer BGP a través de una conexión L2TP
  - la nomenclatura propuesta para cada conexión es **lbgp** seguido del servidor remoto a conectar, por ejemplo: **lbgpexo** para especificar que es un peer BGP sobre L2TP hacia el servidor de túneles comunitarios exo
- interfaz ethbgp:
  - se establece un peer BGP a través de una conexión ethernet
  - la nomenclatura propuesta para cada conexión es **ethbgp** seguido del nodo bgp remoto, por ejemplo: **ethbgpbcncalle13** para especificar que es un peer BGP sobre ethernet hacia el bgp remoto de BCN Calle 13

```mermaid
flowchart LR

subgraph bmx6net [bmx6 network]
direction LR
mesh1---mesh2
mesh1---mesh3
mesh2---mesh3
end

subgraph bgpnet [BGP network]
direction LR
supernode1---supernode2
supernode1---supernode3
supernode2---supernode3
node1-.->supernode1
node2-.->supernode1
node3-.->supernode1
node4-.->supernode2
node5-.->supernode2
node6-.->supernode3
end

bmx6net-- bmx6 iface ---device
bgpnet-- ethbgp iface ---device
device--  wan iface ---internet
internet---l2tpserver[L2TP server]
```

escenarios:

- l2tp
- enlace con nodo infraestructura

ficheros:

- /etc/config/bird4
- /etc/bird4/filters/filter-generic
- /etc/bird4/functions/function-generic
