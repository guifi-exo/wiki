# Ventoy

## Script helper to install ventoy

[ventoy](https://www.ventoy.net) allows you to reuse a USB with different ISO images. Once installed,
when you plug it into your PC you will see a folder to upload the ISOs you need

here is a simple script to install ventoy on your USB, run each lines consciously:

```sh
latest_url="https://api.github.com/repos/ventoy/Ventoy/releases/latest"
VERSION=$(curl -s "${latest_url}" | grep tag_name | cut -d '"' -f 4) \
  && echo "Latest Ventoy version is $VERSION"

wget https://github.com/ventoy/Ventoy/releases/download/${VERSION}/ventoy-$(echo $VERSION | cut -c2-)-linux.tar.gz
tar xvf ventoy-*-linux.tar.gz
cd ventoy-*/
# beware of installing it in the correct device
# substitute /dev/sdx with the proper path to the usb device
#   try with `lsblk` command
sudo ./Ventoy2Disk.sh -i /dev/sdx
```

## Configure ventoy to work on serial

Sometimes, we want to use it on devices such as on APU2

on the visible directory that ventoy creates during the install do the
following commands:

```sh
# navigate to your USB
cd /path/to/ventoy/usb
# create ventoy directory
mkdir ventoy
# create ventoy.json with the following content
cat ventoy/ventoy.json <<END
{
    "theme": {
        "display_mode": "serial_console",
        "serial_param": "--unit=0 --speed=115200"
     }
}
END
```

more info about themes: https://www.ventoy.net/en/plugin_theme.html
