Aquí trobareu recursos adicionals relacionats amb el document de treball: [Pla d'Igualtat de gènere 2020, eXO](https://gitlab.com/guifi-exo/public/-/tree/master/documents-wip/PlaGenere2020.md)

[recursos-comentats.md](./recursos-comentats.md)

enllaços no classificats:

- https://pangea.org/blog-pangea/xarxeta/
- https://ajuntament.barcelona.cat/bcnantimasclista/es/prevenir-y-actuar/guia-de-lenguaje-no-sexista
- https://ajuntament.barcelona.cat/dones/sites/default/files/documentacio/pla-igualtat-2020-2023.pdf
- https://bcnroc.ajuntament.barcelona.cat/jspui/bitstream/11703/113599/1/Protocol%20assetjament%20sexual%202019.pdf
- https://ajuntament.barcelona.cat/dones/sites/default/files/documentacio/quaderns_de_les_dones._assetjament_sexual.pdf
- https://ajuntament.barcelona.cat/dones/sites/default/files/documentacio/quadern_politica_cultura2_baja.pdf
- http://www.centroexil.org/media/contents/actualidad/pla_digualtatexil.pdf
- http://dones.gencat.cat/web/.content/02_institut/10-pla_interdepartamental/Pla_Estrategic_2019_2022.pdf
- https://www.diba.cat/documents/116331112/228537231/Guia+no+discriminacions/f2d42972-8c11-407f-a88a-0ca71b1f7f5a
- http://www.aqu.cat/doc/doc_19381922_1.pdf
- http://www.distintiudegenere.cat/docs/EBOOK-SG%20CITY%2050-50_Cat.pdf
- http://repositori.uji.es/xmlui/bitstream/handle/10234/180739/TFM_2018_AlbeldaSanchis_CarlaM.pdf?sequence=4&isAllowed=y
- http://www.tjussana.cat/doc/publicacions/UP_26.pdf
- https://www.giswatch.org/en/country-report/womens-rights-gender/spain
- https://www.apc.org/es/pubs/principios-feministas-para-internet-version-2
- https://labekka.red/servidoras-feministas/2019/10/09/fanzine-parte-1.html

en general, organitzacions d'interès sobre la temàtica:

- Associació de Dones Investigadores i Tecnòlogues a Catalunya: http://www.amit-cat.org/
- Dones Tech: https://www.donestech.net
- Dones en Xarxa: http://www.donesenxarxa.cat/
- Espai de Dones de Pangea: http://www.pangea.org/dona/espai_de_dones.htm
- Associació 50a50 Lideratge Compartit:http://www.50a50.org/
- Club de Dones Politècniques: https://donespolitecniques.com/qui-som/on-volem-anar/
- The European Association for Women in Science, Technology, Engineering and Mathematics (STEM): http://www.genderportal.eu/organisations/european-association-women-science-engineering-technology
- International mentorship group: http://www.pyladies.com/
- Mujeres Mapeando El Mundo: https://geochicas.org/
- Fil a l'agulla. Acompanyen persones i grups a ser mes qui son: https://filalagulla.org/
- https://www.giswatch.org/en/country-report/womens-rights-gender/spain
- https://www.colectic.coop/
- https://www.dabne.net/es/home
- Llista d'entitats potencialment d'interès https://gitlab.com/guifi-exo/wiki/blob/master/basic/regeneracio.md (Lauro)

