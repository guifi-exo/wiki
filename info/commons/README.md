information about how to implement a commons system (except digital commons)

note: I think it would be nice to start from the Elinor Ostrom research and from there where we arrive

- [polycentrism](polycentrism.md)
- [property](property.md)

related (and unclassified)

- [About IETF governance](https://www.wired.com/1995/10/ietf/)
