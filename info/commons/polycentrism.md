# Polycentrism

A political or cultural system which contains many different centres, especially centres of authority or control -> https://en.wiktionary.org/wiki/polycentrism#English

"Her proposal was that of a polycentric approach, where key management decisions should be made as close to the scene of events and the actors involved as possible" -> https://en.wikipedia.org/wiki/Elinor_Ostrom#Environmental_protection

Articles:

- Do we really want to consolidate urban police forces? A reappraisal of some old assertions. Elinor Ostrom et al.
- Beyond Markets and States: Polycentric Governance of Complex Economic Systems. Elinor Ostrom et al.
- Polycentricity: From Polanyi to Ostrom, and Beyond. PAUL D. ALIGICA and VLAD TARKO
- [Polycentricity and Local Public Economies](https://web.archive.org/web/20130403071654/http://www.indiana.edu/~workshop/publications/materials/volume2.html)
- Polycentric Systems of Governance: A Theoretical Model for the Commons. Keith Carlisle, Rebecca L. Gruby
