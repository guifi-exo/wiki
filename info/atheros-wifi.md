<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [ath10k](#ath10k)
  - [known bugs](#known-bugs)
- [ath9k](#ath9k)
  - [frequency jamming](#frequency-jamming)
  - [debug](#debug)
    - [dfs](#dfs)
  - [adjusting to custom bandwidth](#adjusting-to-custom-bandwidth)
- [ath9k vs ath10k](#ath9k-vs-ath10k)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->
information about atheros wifi

# ath10k

- ath10k: open source part of the wireless driver included in linux kernel https://wireless.wiki.kernel.org/en/users/drivers/ath10k requires binary blob ath10k-firmware that is closed source
- ath10k-firmware is closed source, [but candela technologies (ct) had access to the ath10k source code](https://www.candelatech.com/ath10k.php), so through them development and [bugfixing](https://forum.openwrt.org/t/why-the-switch-to-unstable-ath10k-ct/27258/2) can be done. That's why openwrt uses ath10k-ct by default.
  - ath10k-firmware-qcaNNNNy : this is the official driver, only supports 802.11s mesh mode, don't use the 802.11s routing protocol implemented in this ath10k-firmware
    - one person said: "I have been working with this firmware since months with no issues"
  - ath10k-firmware-qcaNNNN-ct : they worked to have "IBSS [Adhoc] and other features". This is the default ath10k driver that is being used in openwrt, because its developer is close to the openwrt community, means that [bugs can be fixed](https://forum.openwrt.org/t/why-the-switch-to-unstable-ath10k-ct/27258)
    - In eXO we found in july-august 2019 that adhoc-ibss had bad performance on 802.11ac devices, that motivated us to start switching our network to 802.11s
    - [802.11s is not really supported by ath10k-ct](https://github.com/greearb/ath10k-ct/issues/81)
  - ath10k-firmware-qcaNNNN-ct-htt : "Uses normal HTT TX data path for management frames (...)", I don't know nobody that uses it
  - kmod-ath10k-ct-smallbuffers: [officially used for ath10k devices of 64 MB of ram](https://www.mail-archive.com/openwrt-devel@lists.openwrt.org/msg50150.html), this problem does not affect official ath10k, but requires testing

TODO: https://www.candelatech.com/ath10k-10.1.php vs https://www.candelatech.com/ath10k-10.4.php

## known bugs

- after adjusting wifi settings you can get: `pdev param 0 not supported by firmware`. Ignore that kernel message
- do not believe what says *RX rate* and/or *TX rate*, specially, if you get 6 Mbit, that is not true, that is an unfixed bug. Use another tools to measure your wireless link such as signal strenght or `iperf3` between the antennas
- sometimes the firmware crashes, recent kernels auto-reload the firmware. which includes 19.x release, proof:
  - mainline ath10k in master was affected while backport 4.19/5.4 were in use - but we're now using backports 5.7, which fixes that issue; and ath10k-ct (which is the default) wasn't affected at all. so basically, all fine now
  - wireless drivers are using backports, the base kernel doesn't matter (much), `dmesg | grep backp` should tell you the version (but 19.07.x comes with 4.14, which is fine) both ath10k and ath10k-ct are autoloaded depending on which you have installed

# ath9k

## frequency jamming

TODO: enable option tx99 and try if it jams frequency

also known as TDNA (Time Division No Access) ;)

## debug

access to interesting stuff

    cd /sys/kernel/debug/ieee80211/phy0/ath9k

### dfs

[DFS](https://en.wikipedia.org/wiki/Channel_allocation_schemes#DFS) is a mandatory regulation procedure that applies on some channels of 5 GHz band. Its goal is avoid Wifi channel occupation if any other preferent service is present.

see if there is a radar

    cat dfs_stats

example output of `dfs_stats` without radars detected

```
DFS support for macVersion = 0x80, macRev = 0x2: enabled
Pulse detector statistics:
    pulse events reported    :          0
    invalid pulse events     :          0
    DFS pulses detected      :          0
    Datalen discards         :          0
    RSSI discards            :          0
    BW info discards         :          0
    Primary channel pulses   :          0
    Secondary channel pulses :          0
    Dual channel pulses      :          0
Radar detector statistics (current DFS region: 0)
    Pulse events processed   :          0
    Radars detected          :          0
Global Pool statistics:
    Pool references          :          0
    Pulses allocated         :          0
    Pulses alloc error       :          0
    Pulses in use            :          0
    Seqs. allocated          :          0
    Seqs. alloc error        :          0
    Seqs. in use             :          0

```

simulate a radar, as a way to test the wifi channel change

    echo 1 > dfs_simulate_radar

## adjusting to custom bandwidth

`chanbw` part allows you to subdivide the bandwidth of the channel to 5 MHz, this way, energy gets more concentrated. "this is how you can reach 100 km". TODO review how to use it (probably reading the linux kernel source code)

TODO: find how to use chan


# ath9k vs ath10k

person1: I would not suggest to attempt to use ath10k based devices, both on ethical (it need binary blobs to work) and on pragmatic concerns (it probably won't work out of the box and most probably even after custon configuration won't have good behaviour/performances)

person2: I am using ath10k and looks like works better than ath9k (...) ath10k is not an anomaly, upcoming ath11k will work in the same direction as ath10k
