broadband-forum.org does technical reports that are useful references for deploy networks

propose standard of how to deploy the infrastructure for optical fiber with GPON: https://www.broadband-forum.org/technical/download/TR-156_Issue-4.pdf

looks like TR-069 evolves to [USP](https://usp.technology/). [USP webinar](https://drive.google.com/file/d/1kSxyvSU5gUlykmwQoNyxOPkcHHYWj1I8/view)

other technical reports https://www.broadband-forum.org/technical-reports

other resources (more generic) https://www.broadband-forum.org/broadband-forum-resources
