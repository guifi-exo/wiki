un node a guifi és un emplaçament on hi han els equips de telecomunicacions [amb la llicència de comuns](https://guifi.net/ComunsXOLN) que fan possible la connexió a la xarxa guifi.net

per crear un node, primer has de registrar un usuari o entrar, en la pàgina https://guifi.net

![](registrar-usuari-guifi.png)

un cop hem entrat amb el nostre usuari "creem contingut"

![](crear-contenido.png)

i després "creem el node"

![](crear-nodo.png)

a continuació hi ha un formulari i entenc que a partir d'aquí ja és més fàcil
